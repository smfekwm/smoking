import React from "react";
import moment from "moment";

export class Status extends React.Component {
  constructor(props) {
    super(props);

    this.handleMouseHover = this.handleMouseHover.bind(this);
    this.time = this.time.bind(this);
    this.state = {
      isHovering: false
    };
  }

  handleMouseHover(e) {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return {
      isHovering: !state.isHovering
    };
  }

  time() {
    
  }

  render() {
    return this.props.smokerStatus === "ex-smoker" ? (
        <div>
        <p className="tooltip">
          Ex-smoker
          <span className="tooltiptext">
            Last cigarette {moment(this.props.quitDate).fromNow("dd")} ago
          </span>
        </p>
        </div>
    ) : (
      <div>
      <p className="tooltip">Smoker</p>
      </div>
    ); 
  }
}


export default Status;
