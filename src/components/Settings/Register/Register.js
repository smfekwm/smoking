import React from "react";
import "./Register.css";
import { Route, Redirect } from 'react-router';



class Policy extends React.Component {
  render() {
    return <div className="register-box">
      <p>By register you agree to Terms of use, Privacy Policy.</p>
      <p>Cookies</p>
        </div>;
  }
}



class Warning extends React.Component {
  render() {
    return <p className="warning-text"> {this.props.warning}</p>;
  }
}

class RegisterForm extends React.Component {
  render() {
    return (
      <form className="register-box" onSubmit={this.props.handleSubmit}>
        <label className="register-label">
          <p>Name:</p>
          <input
            className={this.props.userClass}
            name="username"
            type="text"
            placeholder="4-22 characters"
            value={this.props.username}
            onChange={this.props.handleChange}
          />
        </label>
        <label className="register-label">
          <p>Password:</p>
          <input
            className={this.props.passwordClass}
            name="password"
            type="password"
            placeholder="min. 4 characters"
            value={this.props.password}
            onChange={this.props.handleChange}
          />
        </label>
        <label className="register-label">
          <p>Repeat password:</p>
          <input
            className="register-input"
            name="repeatedPassword"
            type="password"
            value={this.props.repeatedPassword}
            onChange={this.props.handleChange}
          />
        </label>
        <label className="register-label">
          <p>Write answer:</p>
          <div className="register-label-row">
          <p> {this.props.randomNumber} x 2 = </p> 
          <input
          className={this.props.answerClass}
            name="answer"
            type="text"
            value={this.props.answer}
            onChange={this.props.handleChange}
          />
          </div>
        </label>
        {this.props.isLoading
        ? <button className="button-long"><i className="fa fa-spinner fa-spin" />
        </button>
        : <button className="button-long" onClick={this.handleSubmit}>Register</button>
        }
        <Warning warning={this.props.warning} />

      </form>
    );
  }
}

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      repeatedPassword: "",
      warning: "",
      userClass: "register-input input-username",
      passwordClass: "register-input input-password",
      answerClass: "register-input-small input-answer",
      randomNumber: null,
      answer: null,
      isSpam: true,
      isLoading: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.post = this.post.bind(this);
    this.checkPasswordMatch = this.checkPasswordMatch.bind(this);
    this.checkAnswer = this.checkAnswer.bind(this);
    this.setRandomNumber = this.setRandomNumber.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.wrongAnswer = this.wrongAnswer.bind(this);
    this.wrongUser = this.wrongUser.bind(this);
    this.wrongPassword = this.wrongPassword.bind(this);
    this.resetWarningInput = this.resetWarningInput.bind(this);
    this.registered = this.registered.bind(this);
    this.registerUser = this.registerUser.bind(this);
    this.failedRegistration = this.failedRegistration.bind(this);

    
  }

  post(url, content, callback, callback2) {
    this.setState({ isLoading: true }) 
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          
          if (xhr.responseText) {
            if (xhr.responseText === "authenticated") {
              callback.call();
            } else {
              callback2.call();
            }
          }
        } else {
          console.error(xhr.statusText);
        }
      }
    };
    xhr.onerror = function(e) {
      console.error(xhr.statusText);
    };
    xhr.open("POST", url, true);

    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.withCredentials = true;

    xhr.send(
      JSON.stringify({
        content
      })
    );
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    this.setState({ [name]: event.target.value });
  }


  handleSubmit(event) {
    event.preventDefault();
    this.resetWarningInput();
    this.validateForm();
  }

  setRandomNumber () {
    let date = Date.now().toString()
    let number = date.slice(date.length -1, date.length)
    this.setState({ randomNumber: number })
  }


  resetWarningInput() {
    this.setState({ userClass: "register-input input-username",
      passwordClass: "register-input input-password",
      answerClass: "register-input-small input-answer", warning: ""})
  }

  wrongPassword(text) {
    document.querySelector(".input-password").focus()
      this.setState({ warning: text, passwordClass: "register-input input-password warning-input" });
      return;
  }

  wrongAnswer() {
    document.querySelector(".input-answer").focus();
    this.setState({ warning: "Answer is not correct.", answerClass: "register-input input-answer warning-input"})
    return;
  }


  wrongUser(text) {
    document.querySelector(".input-username").focus()
    this.setState({
      warning: text, isLoading: false, userClass:"register-input input-username warning-input"
    });
    return;
  }

  validateForm() {
    if (this.state.username.length < 4) {
      this.wrongUser("Your username is too short");
    } 
    else if (this.state.username.length > 23) {
      this.wrongUser("Your username is too long");
    }
    else if (this.state.password.length < 4) {
      this.wrongPassword("Your password is too short");
    } 
    else {
      this.checkPasswordMatch()
    }
      
  }

  checkPasswordMatch() {
    this.state.password === this.state.repeatedPassword 
    ? this.checkAnswer()
    : this.wrongPassword("Your passwords didn't match.")
  }

  checkAnswer() {
    this.state.answer == this.state.randomNumber * 2 
    ? this.registerUser()
    : this.wrongAnswer()
  }

  registerUser() {
    this.post(
      "https://ventpuff.com/register",
      {
        username: this.state.username,
        password: this.state.password,
      }, this.registered, this.failedRegistration
    )
  }

  registered() {
    this.setState ({ warning: "Success! You will be redirected"})
    setTimeout(this.props.loggUser, 5000)
  }

  failedRegistration() {
    this.wrongUser("User already exists")
  }



  componentDidMount() {
    this.setRandomNumber()
    
  }
  componentDidUpdate() {
    
  }

  render() {
    return (
      <div className="register-wrapper">
      {this.props.isLogged ? (
          
          <Redirect to="/feed"/>
        ) :null }
        
        
          <RegisterForm
            username={this.state.username}
            password={this.state.password}
            repeatedPassword={this.state.repeatedPassword}
            randomNumber={this.state.randomNumber}
            answer={this.state.answer}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}checkAnswer={this.checkAnswer}
            isLoading={this.state.isLoading}
            userClass={this.state.userClass}
            passwordClass={this.state.passwordClass}
            answerClass={this.state.answerClass}
            warning={this.state.warning}
          />
        
       
        <Policy />
      </div>
    );
  }
}

export default Register;
