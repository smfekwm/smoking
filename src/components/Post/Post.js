import React from "react";
import moment from "moment";

import Posts from "../Feed/Posts/Posts";






class LoadedPost extends React.Component {
  
  render() {
    const posts = this.props.data.map(post => (

      <Posts
        key={`post${post.id}`}
        postId={post.id}
        type="post"
        value="post"
        username={post.username}
        usernameToProfile={`/profile/${post.username}`}
        avatar={`https://ventpuff.com/uploads/${post.avatar}`}
        text={post.text}
        smokerStatus={post.smokerStatus}
        quitDate={post.quitDate}
        comments={post.comments !== null ?post.comments 
        : [] }
        likes={post.postLikes}
        liked={post.postLikes !== null ?
        ((post.postLikes.map(like => like.username)).includes(this.props.user)
        ? true
        : false
        )
        : false   
        }
        timestamp={moment(parseInt(post.timestamp, 10))
              .startOf("minutes")
              .fromNow()}
        tag={post.tag}
        image={post.image}
        user={this.props.user}
        isLogged={this.props.isLogged}
        fetchData={this.props.loadMoreData}
        updateData={this.props.updateData}
        getDataTag={this.props.getDataTag}
        handleSubmitReport={this.props.handleSubmitReport}


      />
    ))
    return (

        <div className="feed-wrapper">
          {posts}
        </div>
    )
  }
}






class PostContent extends React.Component {

  render() {
    return (
    
    this.props.data.length > 0

    ? <LoadedPost data={this.props.data} updateData={this.props.updateData} 
    isLogged={this.props.isLogged}
    user={this.props.user}
       />


    : <div>
      <h3>Error: This post does not exist</h3>
    </div>
    )
}
}






class Post extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: [],
    };
    this.getDataTag = this.getDataTag.bind(this);
    this.getData = this.getData.bind(this);
    this.updateData = this.updateData.bind(this);


  }

  getDataTag(event) {
    let targetTag = event.target.textContent.slice(1);

    this.setState({ isLoading: true });
    if (targetTag === "question") {
      let url = "https://ventpuff.com/fetch/question";
      fetch(url, {
        method: "GET",
        credentials: "include"
      })
        .then(response => response.json())
        .then(dataset => this.setState({ data: dataset, isLoading: false }));
    } else if (targetTag === "advice") {
      let url = "https://ventpuff.com/fetch/advice";
      fetch(url, {
        method: "GET",
        credentials: "include"
      })
        .then(response => response.json())
        .then(dataset => this.setState({ data: dataset, isLoading: false }));
    }
  }

  updateData() {
    let postIdRoute = this.props.match.params.postId;
    
    const url = `https://ventpuff.com/fetch/post/${postIdRoute}`;

    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => dataset.id !== "none"
        ? this.setState({ data: dataset})
        :  null )
  }

  getData(post) {
   
    this.setState({ isLoading: true });
    const url = `https://ventpuff.com/fetch/post/${post}`;

    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => dataset.id !== "none"
        ? this.setState({ data: dataset, isLoading: false })
        : this.setState({ data: [], isLoading: false })
      );
    
  }


  componentDidMount() {
  
    let postIdRoute = this.props.match.params.postId;
    this.setState({ currentRoute: postIdRoute })

    this.getData(postIdRoute)
  }


  render() {

    return (
      <div className="post-wrapper" >
        {this.state.isLoading
          ? <h3>Loading</h3>
          : <PostContent 
          data={this.state.data}
          updateData={this.updateData}
          isLogged={this.props.isLogged}
          user={this.props.user}
           />
        }


      </div>
    );
  }
}

export default Post;
