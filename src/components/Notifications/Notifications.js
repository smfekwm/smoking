import React from "react";
import "./Notifications.css";
import { Link } from 'react-router-dom';
import moment from "moment";
import { NavLink } from "react-router-dom";
import Loading from "../Loading/Loading";
import {sendPost} from '../../functions.js';




class Notification extends React.Component {
  render() {
    return (  
    <NavLink to={this.props.notificationToPost}
    onClick={this.props.openNotification}
     className={this.props.viewClass}      
    parrentsubcomment={this.props.parrentsubcomment}
    type={this.props.type}
    notificationid={this.props.id}
    parrentid={this.props.parrent}>
      <p className="notification-header">{this.props.timestamp}</p>
        <div className="notification-avatar"><img alt="profile-avatar" className="img-avatar" src={this.props.avatar}/></div>
        <p className="notification-text"><b> {this.props.username}</b> has
          {this.props.type === "like" 
          ? " liked your post"
          : null
          }
          {this.props.type === "comment" 
          ? " commented your post"
          : null
          }
          {this.props.type === "subcomment" 
          ? " reacted to your comment"
          : null
          }
          
          </p>
      
      </NavLink>
    )}
}

class Notifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notificationsData: [],
      notificationPost: null,
      selectedNotification: "",
      isLoading: false,
    };
    this.checkNotifications = this.checkNotifications.bind(this);
    this.clearNotifications = this.clearNotifications.bind(this);

  }

  checkNotifications() {
    this.setState({ isLoading: true })
    const url = "https://ventpuff.com/fetch/notifications";
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ notificationsData: dataset, isLoading: false }));

      this.props.checkNewNotifications()
  }
  
  clearNotifications() {
    this.setState({ isMarkingRead: true })
    sendPost("https://ventpuff.com/clear/notifications", "",this.checkNotifications,
    this.checkNotifications)
  }

  componentDidMount() {
    this.checkNotifications()
  
  }
  
  render() {
      let newDataSorted = this.state.notificationsData.sort((a, b) => ( b.timestamp > a.timestamp) ? 1 : (( a.timestamp > b.timestamp) ? -1 : 0));

      const notifications = newDataSorted.map(notification => (
        <Notification
        key={`notification${notification.id}`}
        id={notification.id}
        username={notification.username}
        avatar={`https://ventpuff.com/uploads/${notification.avatar}`}
        type={notification.type}
        timestamp={moment(parseInt(notification.timestamp, 10))
          .startOf("minutes")
          .fromNow()}
          viewed={notification.viewed}
          viewClass={`notification-box view-${notification.viewed}`}
        parrent={notification.parrent}
        parrentsubcomment={notification.parrentsubcomment ? notification.parrentsubcomment : null}
        openNotification={this.props.openNotification}
        notificationToPost={`/post/${notification.parrent}`}
        />
      ))
    return (
        <div className="notifications-wrapper">
        
        {this.state.notificationsData.length < 1
        ?        <ul className="notifications">
        <div className="notifications-wrapper-header">
        <Link to="settings/notifications" className="button-notifications align-left link-big">
        Settings</Link>
        </div>
        {this.state.isLoading 
        ? <Loading loadingClass="loading-notifications" />
        :  <p className="notifications-empty">No notifications</p>
        }
       
       </ul>
        :       <ul className="notifications">
          <div className="notifications-wrapper-header">
        <Link to="settings/notifications" className="button-notifications align-left link-big">
        Settings</Link>
        
        <a className="button-notifications align-right link-big" onClick={this.clearNotifications}>Mark all read</a>
          
          </div>
          {this.state.isLoading 
        ? <Loading loadingClass="loading-notifications" />
        : null
        }
        {notifications}
          </ul>

        }
        
        </div>
        
    );
  }
}




export default Notifications;
