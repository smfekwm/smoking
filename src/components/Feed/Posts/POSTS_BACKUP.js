import React from "react";
import "./Posts.css";
import Input from "./Comments/Input/Input";
import Comments from "./Comments/Comments";
import Like from "./Like/Like";
import Edit from "./Edit/Edit";
import Delete from "./Delete/Delete";
import EditForm from "./Edit/EditForm";
import moment from "moment"

function post(url, content) {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.withCredentials = true;
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(
    JSON.stringify({
      content
    })
  );
}

class PostsText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLong: false,
      isExpanded: false
    };
    this.isExpanded = this.isExpanded.bind(this);
  }

  isLong() {
    this.props.text.length > 300
      ? this.setState({ isLong: true })
      : this.setState({ isLong: false });
  }

  isExpanded() {
    this.state.isExpanded
      ? this.setState({ isExpanded: false, isLong: true })
      : this.setState({ isExpanded: true, isLong: false });
  }

  componentDidMount() {
    this.isLong();
  }
  render() {
    return (
      <div className="post-text">
        {this.state.isLong ? (
          <div>
            {this.props.text.slice(
              0,
              this.props.text.slice(0, 300).lastIndexOf(" ")
            )}
            {"... "}
            <div className="show-more" onClick={this.isExpanded}>
              (more)
            </div>
          </div>
        ) : (
            this.props.text
          )}
      </div>
    );
  }
}

class Status extends React.Component {
  constructor(props) {
    super(props);
    this.time = this.time.bind(this);

  }

  time() {
    
    
    let height = document.querySelector(".feed-wrapper")
    
   
  }

  render() {
    return (
      
      this.props.smokerStatus === "false"
      ? <div onMouseEnter={this.time}className="smoke-status">Ex-smoker</div>
      : <div className="smoke-status">Smoker</div>
      
    )
  }}

class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showComments: false,
      showLikes: false,
      isLiked: false,
      isEdited: false,
      confirmDelete: false,
    };

    this.confirmDelete = this.confirmDelete.bind(this);
    this.postDelete = this.postDelete.bind(this);
    this.isEdited = this.isEdited.bind(this);
    this.isLiked = this.isLiked.bind(this);
    this.showComments = this.showComments.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  showComments() {
    this.state.showComments
      ? this.setState({
        showComments: false
      })
      : this.setState({ showComments: true });
  }

  isLiked() {
    this.state.isLiked
      ? this.setState({ isLiked: false })
      : this.setState({ isLiked: true })
    post("/like", { id: this.props.postId });
    setTimeout(this.props.updateData, 500);
  }

  postLike() {

  }

  isEdited() {
    this.state.isEdited
      ? this.setState({
        isEdited: false
      })
      : this.setState({ isEdited: true })
  }

  confirmDelete() {
    this.state.confirmDelete
      ? this.setState({ confirmDelete: false })
      : this.setState({ confirmDelete: true })
  }

  postDelete() {
    post("/delete", { postId: this.props.postId });
    setTimeout(this.props.updateData, 500);
  }

  handleClick(e) {
    e.preventDefault();
    
  }


  componentDidMount() {
    // check in DB for posts which curent user liked
    this.props.allLikes.map(
      like =>
        like === this.props.loggedUser
          ? this.setState({
            isLiked: true
          })
          : this.setState({ isLiked: false })
    );
  }


  render() {
    const commentsData = this.props.commentsDataset.map(comment => (
      <Comments
        key={comment.commentId}
        commentId={comment.commentId}
        commentText={comment.text}
        commentUsername={comment.username}
        commentDate={moment(comment.timestamp).startOf("day").fromNow()}
      />
    ));

    return (
      <div className="post-box">
        <div className="post-header">
          <div className="post-username">{this.props.username}
          </div>
          {this.props.smokerStatus.length > 1
          ? <Status smokerStatus={this.props.smokerStatus} quitDate={this.props.quitDate} />
          : null
          }
        
          <div>
            {moment(this.props.quitDate).startOf("day").fromNow()}
          </div>
          <div className="post-date">{moment(this.props.timestamp).startOf("day").fromNow()}</div>
        </div>
        {this.state.isEdited
          ? <div className="post-text">
            <EditForm postText={this.props.text} postId={this.props.postId} updateData={this.props.updateData} isEdited={this.isEdited} />
          </div>
          : <PostsText text={this.props.text} />
        }
        <div className="post-footer">
          <div className="post-likes">
            <div className="post-heart">
              <Like
                handleClick={this.isLiked.bind(this)}
                isLiked={this.state.isLiked}
              />
            </div>
            {this.props.likesCount ? (
              <div className="post-likes-text">
                {this.props.likesCount}
              </div>
            ) : null}
          </div>
          <div className="post-comment-list" onClick={this.showComments}>
            <i className="far fa-comment" >{"  "}{this.props.commentsCount}</i>
          </div>
          {this.props.loggedUser === this.props.username ? (
            <div className="post-edit">
              <Edit handleClick={this.isEdited} />
              <Delete handleClick={this.confirmDelete} />
            </div>
          ) : null}
        </div>
        {this.state.confirmDelete
          ?
          <div className="delete-dialog">
            <p>Do you want to delete your post?</p>
            <button onClick={this.postDelete}>Yes</button>
            <button onClick={this.confirmDelete} >No</button>
          </div>
          : null
        }
        {this.state.showComments ? (
          this.props.commentsCount > 0 ? (
            <div>
              <div className="input-box">
                <Input route="/comment" postId={this.props.postId} updateData={this.props.updateData} />
              </div>
              <div className="comments-wrapper">{commentsData}</div>
            </div>
          ) : (
              <div className="input-box">
                <Input route="/comment" postId={this.props.postId} />
              </div>
            )
        ) : null}
      </div>
    );
  }
}

export default Posts;
