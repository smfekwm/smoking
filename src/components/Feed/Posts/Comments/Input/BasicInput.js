import React from 'react';
import './Input.css';
import TextareaAutosize from 'react-textarea-autosize';
import {sendPost} from '../../../../../functions.js';




class Options extends React.Component {
  render() {
    return (
      <p className="input-box-expanded-warning">
       Max characters: {this.props.charactersLeft}
      </p>
    );
  }
}




class BasicInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      textFormValue: "",
      isPosting: false,
      isFocused: false,
      charactersLeft: 300,

  };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.isPosting = this.isPosting.bind(this);
    this.finishPosting = this.finishPosting.bind(this);
    this.showError = this.showError.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.countCharacters = this.countCharacters.bind(this);


  }


   
  countCharacters() {
    let postLength = this.state.textFormValue.length
    let charactersCount = 300 - postLength
    this.setState({ charactersLeft: charactersCount })
  }

  isPosting () {
    this.state.isPosting
    ? this.setState({ isPosting: false })
    : this.setState({ isPosting: true })
  }

  finishPosting () {
    this.props.updateData()
    this.isPosting()
  }

  showError() {
    
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    this.setState({[name]: event.target.value});
    this.countCharacters();
  }

  handleFocus() {
    
    this.setState({ isFocused: true })
  }

  handleSubmit(event) {
    event.preventDefault();
    this.isPosting();
    sendPost(this.props.route, {userPost: this.state.textFormValue, postId: this.props.postId}, this.finishPosting, this.showError)
    this.setState({ textFormValue: "", isFocused: false, charactersLeft: 300 })
  }

    render() {
      return (
        this.props.isLogged
        ?  <div>
          <form className="post-input-small" onSubmit={this.handleSubmit}>
        <TextareaAutosize
          className="textarea-post main-post"
          minRows={1}
          maxRows={4}
          resize="none"
          placeholder="Write something..."
          name = "textFormValue"
          autoComplete="off"
          value={this.state.textFormValue}
          onFocus={this.handleFocus}
          onChange={this.handleChange} />
          <div className="sendingIcon ">
          {this.state.isPosting
          ? (
              <i className="fa fa-spinner fa-spin" />
          ) 
          : <i className="fas fa-paper-plane fa-rotate-45" onClick={this.handleSubmit} />
          }
        </div>
      </form>
      {this.state.isFocused
      ? <Options 
      charactersLeft={this.state.charactersLeft}
      />
      : null
      }
      </div>
        :  <form className="post-input-small" >
        <TextareaAutosize
          className="textarea-post comment-disabled"
          minRows={1}
          resize="none"
          placeholder="Please, login to comment post"
          value=""
          name = "textFormValue"
          autoComplete="off"
          />
          <div className="sendingIcon ">
          <i className="fas fa-angle-right fa-2x" />
          
        </div>
      </form>
      
      );
    }
  }

  export default BasicInput;
