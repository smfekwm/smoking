import React from "react";
import { Link } from 'react-router-dom'
import moment from "moment"
import { Route, Redirect } from 'react-router';
import "./Settings.css";
import { NavLink } from "react-router-dom";


class BlockedSettings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      blockedUsers: []
    };
    this.fetchBlockedUsers = this.fetchBlockedUsers.bind(this);
  }

  fetchBlockedUsers() {
    const url = "https://ventpuff.com/fetch/blocked";
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ blockedUsers: dataset}));
  }

  componentDidMount() {
    
  }

  render() {
    return (
      <ul className="blocked-list">asf
      <li className="blocked-user">aaa</li>
      </ul>
    )
  }
}

export default BlockedSettings;
