import React from "react";
import { Link } from 'react-router-dom'
import QuitDateForm from "./QuitDateForm/QuitDateForm";
import "./Settings.css";
import {sendPost} from '../../functions.js';
import TextareaAutosize from 'react-textarea-autosize';
import Compressor from 'compressorjs';

class Avatar extends React.Component {
  render() {
    return (
      <div className="settings-profile-avatar">
        {this.props.isLoadingImage ? (
          <div className="settings-profile-loading-avatar">
            <i className="fa fa-spinner fa-spin avatar-spin-icon" />
          </div>
        ) : null}
        <img alt="profile avatar" className="settings-profile-avatar" src={this.props.avatar} />
        
      
      </div>
    );
  }
}

class Account extends React.Component {
  componentDidMount() {
    this.props.getUserSettingsData();
  }



  render() {
    return (   
      <div>
          <div className="settings-profile-header">
        <Avatar
          avatar={this.props.avatar}
          username={this.props.username}
          isLoadingImage={this.props.isLoadingImage}
          selectedFile={this.props.selectedFile}
        />
        <div className="image-upload">
          <h3 className="settings-username">{this.props.username}</h3>  
            <label className="settings-link" htmlFor="file-input">
             
              Change photo
            </label>

            <input
              onChange={this.props.fileHandler}
              id="file-input"
              type="file"
            />
          </div>

       <p className="settings-account-label">Status </p>
       <SmokeStatus 
        smokerStatus={this.props.smokerStatus} quitDate={this.props.quitDate}
        newSmokerStatus={this.props.newSmokerStatus} newQuitDate={this.props.newQuitDate}
        handleSelectDate={this.props.handleSelectDate} handleSelect={this.props.handleSelect}
        convertTime={this.props.convertTime}
        handleChange={this.props.handleChange}
        handleSubmit={this.props.handleSubmit}
         />

 {this.props.newSmokerStatus === "ex-smoker"
            ? 
            <p className="settings-account-label">Last cigarette </p>

            : null
            }

       {this.props.newSmokerStatus === "ex-smoker"
            ? 
              <QuitDateForm handleChange={this.props.handleChange}
            quitDay={this.props.quitDay}
            quitMonth={this.props.quitMonth}
            quitYear={this.props.quitYear}
            />
            : null
            }

        <p className="settings-account-label">About </p>
        <div>
       <TextareaAutosize
          className = "textarea-edit-clear"
          name="aboutFormValue"
          minRows={1}
          maxRows={11}
          resize="none"
          maxLength={203}
          onChange={this.props.handleChange}
          value={this.props.aboutFormValue} />
          <p className="small-text">Characters left: {this.props.charactersLeftAbout}</p>
          </div>
          </div>
          <div className="settings-footer">
          {this.props.isLoading ? (
          <a className="button-default hidden-text">Save
            <i className="fa fa-spinner fa-spin inside" />
          </a>
        ) : (
          <a className="button-default" onClick={this.props.handleSubmitProfile}>
          Save
          </a>
        )}
          </div>
      </div>
    )
  }
}


class SettingsComponent extends React.Component {
  render() {
    return (        
        <div className="settings-list-box">
          <div className="settings-header">
          <div className="button-back">
          <Link className="link-big" to="/settings/edit">Back
         
            </Link>
          </div>
          <p className="settings-title">{this.props.settingsTitle}</p>
          </div>
          <div className="settings-content">
            {this.props.settingsContent}
            </div>
            </div>
    )
  }
} 


class Notifications extends React.Component {
  componentDidMount() {
    this.props.fetchNotificationsSettings();
  }
  render() {
    return (        
      <div className="notifications-settings">
      <p>Reactions to your posts</p>
      <label class="switch">
  <input className="post_notifications" checked={this.props.postsNotification} onClick={this.props.changeNotificationSettings}type="checkbox"></input>
  <span class="slider round"></span>
</label>
<p>Portal announcements</p>
<label class="switch">
  <input className="web_notifications" checked={this.props.webNotifications} onClick={this.props.changeNotificationSettings}type="checkbox"></input>
  <span class="slider round"></span>
</label>
      </div>
    )
  }
}



class SmokeStatus extends React.Component {
  render() {
    return (
      <div>
      <label className="settings-label">             
              <select className="settings-input" value={this.props.newSmokerStatus} onChange={this.props.handleSelect}>
              <option className="settings-input-content" defaultValue={null}> </option>
                <option className="settings-input-content" value="smoker">Smoker</option>
                <option className="settings-input-content" value="ex-smoker">Ex-Smoker</option>
              </select>
            </label>
           
            
    </div>
    )
  }
}

class Warning extends React.Component {
  render() {
    const warningClass = `warning-text ${this.props.warningClass}`
    return <p className={warningClass}> {this.props.warning}</p>;
  }
}


class PasswordPrompt extends React.Component {
  render() {
    return (
        <div>
          <br/>
        <div className="input-float ">
              <input required
                className="input"
                name="oldPassword"
                type="password"
                value={this.props.oldPassword}
                onChange={this.props.handleChange}
              />
              <span className="label">Old password</span>
              {this.props.oldPasswordWarning
          ? <Warning warning={this.props.oldPasswordWarning} warningClass="invalid" />
          : null 
          }
          <div className="input-icon">
            {this.props.oldPasswordValid
            ? <i class="fas fa-check-circle valid"></i>
            : null
            }
            {this.props.oldPasswordInalid
            ? <i class="fas fa-times-circle invalid"></i>
            : null
            }
            
          </div>
            </div>
        <div className="input-float ">
              <input required
              className="input"
                name="newPassword"
                type="password"
                value={this.props.newPassword}
                onChange={this.props.handleChangePassword}
              />
              <span className="label">New password</span>
              {this.props.newPasswordWarning
          ? <Warning warning={this.props.newPasswordWarning} warningClass="invalid" />
          : null 
          }
              <div className="input-icon">
            {this.props.newPasswordValid
            ? <i class="fas fa-check-circle valid"></i>
            : null
            }
            {this.props.newPasswordInvalid
            ? <i class="fas fa-times-circle invalid"></i>
            : null
            }
            
          </div>
              </div>

              <div className="input-float ">
          <input required
          className="input"
            name="repeatedPassword"
            type="password"
            value={this.props.repeatedPassword}
            onChange={this.props.handleChangePassword}
          />
          <span className="label">Repeat new password</span>
          {this.props.repeatedPasswordWarning
          ? <Warning warning={this.props.repeatedPasswordWarning} warningClass="invalid" />
          : null 
          }
          <div className="input-icon">
            {this.props.repeatedPasswordValid
            ? <i class="fas fa-check-circle valid"></i>
            : null
            }
            {this.props.repeatedPasswordInvalid
            ? <i class="fas fa-times-circle invalid"></i>
            : null
            }
            
          </div>
          </div>

          <br/>
          <br/>
          <div className="settings-footer">
          {this.props.isLoadingPassword ? (
          <a className="button-default hidden-text">Save
            <i className="fa fa-spinner fa-spin inside" />
          </a>
        ) : (
          <a className="button-default" onClick={this.props.handleSubmitPassword}>
            Save
          </a>
        )}
          {this.props.warning
          ? <Warning warning={this.props.warning} warningClass={this.props.passwordWarningClass} />
          : <Warning warning={" "} warningClass={this.props.passwordWarningClass} /> 
          }
            </div>
            </div>
    )
  }
}



class Password extends React.Component {
  
  render() {
    return (        
        <div>
         <PasswordPrompt
         handleSubmitPassword={this.props.handleSubmitPassword}
      oldPassword={this.props.oldPassword}
      newPassword={this.props.newPassword}
      repeatedPassword={this.props.repeatedPassword}
      warning={this.props.warning}
       oldPasswordValid={this.props.oldPasswordValid} 
       oldPasswordInvalid={this.props.oldPasswordInvalid}        newPasswordValid={this.props.newPasswordValid} 
       newPasswordInvalid={this.props.newPasswordInvalid} 
       oldPasswordWarning={this.props.oldPasswordWarning} 
       newPasswordWarning={this.props.newPasswordWarning} 
       handleChangePassword={this.props.handleChangePassword}
       handleChange={this.props.handleChange}
       repeatedPasswordWarning={this.props.repeatedPasswordWarning}
       repeatedPasswordValid={this.props.repeatedPasswordValid}
       repeatedPasswordInvalid={this.props.repeatedPasswordInvalid}
       passwordWarningClass={this.props.passwordWarningClass}
       isLoadingPassword={this.props.isLoadingPassword}
        />
      </div>
    )
  }
}



class OneBlockedUser extends React.Component {

  render() {
    return (
          <li className="li-blocked-user">
            <p className="blocked-user">
          {this.props.username}</p>
          <a className="unblock-user" onClick={this.props.unblockUser} name={this.props.username}>unblock</a>
          </li>
    )}
}

class BlockedUsersList extends React.Component {
  componentDidMount() {
    this.props.fetchBlockedUsers();
  }
  render() {
    const blockedUsers = this.props.blockedUsers.map((user, index)  => (
      <OneBlockedUser
        key={index}
        username={user}
        unblockUser={this.props.unblockUser}
      />
    ));
    return (
      <div>
        <br/>
      {this.props.isLoading 
      ?  <ul className="blocked-list"> <i className="fa fa-spinner fa-spin" /></ul>
      : <div> {this.props.blockedUsers.length > 0
      ? <ul className="blocked-list">
      {blockedUsers}
      </ul>
      : <p>No blocked users</p>
      } </div>
      }
     
      </div>
    )
  }
}

class Delete extends React.Component {
  render() {
    return (
      <div>
        <h5>Do you really want to delete your account and all your posts?</h5>
        <br/>
        <div className="form-float" >

        <div className="input-float">
              <input required
                className="input"
                name="oldPassword"
                type="password"
                value={this.props.oldPassword}
                onChange={this.props.handleChange}
              />
              <span className="label">Enter your password</span>{this.props.passwordWarning
          ? <Warning warning={this.props.passwordWarning} warningClass="invalid" />
          : null 
          }            </div>
        <br/>
        {this.props.isLoading ? (
          <a className="button-default warning-red hidden-text">Delete
            <i className="fa fa-spinner fa-spin inside" />
          </a>
        ) : (
          <a className="button-default warning-red" onClick={this.props.submitDelete}>
            Delete
          </a>
        )}
     </div>
      </div>
    )
  }
}

class SettingsButton extends React.Component {

  render() {
    return (
      <div className="settings-box">
        {this.props.buttonRoute
        ? <Link className="basic-link" to={this.props.buttonRoute}>
       {this.props.buttonName}
            </Link>
        : <a className="basic-link" onClick={this.props.buttonClick}>{this.props.buttonName}</a>
        }
     
      </div>
    )
  }
}

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      newPassword: "",
      repeatedPassword: "",
      oldPasswordValid: false,
      oldPasswordInvalid: false,
      newPasswordValid: false,
      newPasswordInvalid: false,
      repeatedPasswordValid: false,
      repeatedPasswordInvalid: false,
      oldPasswordWarning: "",
      newPasswordWarning: "",
      repeatedPasswordWarning: "",
      passwordWarningClass: "",
      isLoadingPassword: false,
      newSmokerStatus: "",
      newQuitDate: "",
      quitDay: "",
      quitMonth: "",
      quitYear: "",
      userData: [],
      blockedUsers: [],
      isLoading: false,
      isUnblocked: false,
      isLoadingImage: false,
      selectedFile: null,
      aboutFormValue: "",
      charactersLeftAbout: "",
      warning: "",
      postsNotification: "",
      webNotifications: ""
    };
    
    this.logOut = this.logOut.bind(this);
    this.convertTime = this.convertTime.bind(this);
    this.handleSubmitPassword = this.handleSubmitPassword.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSelectDate = this.handleSelectDate.bind(this);
    this.submitDelete = this.submitDelete.bind(this);
    this.fetchBlockedUsers = this.fetchBlockedUsers.bind(this);
    this.unblockUser = this.unblockUser.bind(this);
    this.getUserSettingsData = this.getUserSettingsData.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.fileHandler = this.fileHandler.bind(this);
    this.handleSubmitFile = this.handleSubmitFile.bind(this);
    this.post = this.post.bind(this);
    this.finishUpload = this.finishUpload.bind(this);
    this.handleSubmitProfile = this.handleSubmitProfile.bind(this);
    this.countCharacters = this.countCharacters.bind(this);
    this.validatePassword = this.validatePassword.bind(this);
    this.passwordWasChanged = this.passwordWasChanged.bind(this);
    this.passwordWasWrong = this.passwordWasWrong.bind(this);
    this.handleChangeNotification = this.handleChangeNotification.bind(this);
    this.changeNotificationSettings = this.changeNotificationSettings.bind(this);
    this.fetchNotificationsSettings = this.fetchNotificationsSettings.bind(this);
    this.alterNotificationSettings = this.alterNotificationSettings.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.failedDeleting = this.failedDeleting.bind(this);

    
  }

  handleChangePassword(event) {
    const target = event.target;
    const name = target.name;
    this.setState({ [name]: event.target.value }, this.validatePassword);
  }

  validatePassword() {
    if (this.state.newPassword.length < 8) {
      this.setState({ newPasswordInvalid: true, newPasswordWarning: "Your new password is too short", warning: "" })
    } else {
      this.setState({ newPasswordWarning: "", newPasswordInvalid: false, newPasswordValid: true })
      if (this.state.newPassword === this.state.repeatedPassword) {
        this.setState({ repeatedPasswordInvalid: false, repeatedPasswordValid: true, repeatedPasswordWarning: "", warning: "" })
       } else {
        if (this.state.newPassword.length <= this.state.repeatedPassword.length) {
          this.setState({ repeatedPasswordInvalid: true, repeatedPasswordValid: false, repeatedPasswordWarning: "Your passwords didn't match", warning: "" })
        }
         }
        
    }
  }
  passwordWasChanged() {
    this.setState({ oldPasswordWarning: "", warning: "Your password was changed", oldPassword: "", newPassword: "", repeatedPassword: "", oldPasswordValid: false, newPasswordValid: false, repeatedPasswordValid: false, passwordWarningClass: "valid", isLoadingPassword: false });
  }
  passwordWasWrong() {
    this.setState({ oldPasswordValid: false, oldPasswordInvalid: true,oldPasswordWarning: "Your current password is wrong", oldPassword: "", isLoadingPassword: false });
  }

  handleSubmitPassword() {
    if (this.state.newPasswordValid & this.state.repeatedPasswordValid) {
      this.setState({ isLoadingPassword: true })
      sendPost("https://ventpuff.com/profile/update/password", {
        oldPassword: this.state.oldPassword,
        newPassword: this.state.newPassword
      }, this.passwordWasChanged, this.passwordWasWrong)
    }
   
  }
 

  handleChangeNotification(event) {
    const target = event.target;
    const targetClass = target.className
    let result = document.getElementsByClassName(targetClass)[0].checked ? true : false
    this.setState({ [targetClass]: result });
  }

  changeNotificationSettings(event) {
    let target = event.target;
    let notificationType = target.className
    let result = document.getElementsByClassName(notificationType)[0].checked ? true : false
    let notificationValue = result
    sendPost(`https://ventpuff.com/settings/update/${notificationType}`, {
      notificationValue: notificationValue
    }, this.fetchNotificationsSettings)
  }

  fetchNotificationsSettings(callback) {
    const url = "https://ventpuff.com/fetch/settings/notifications";

    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => dataset 
        ? this.alterNotificationSettings(dataset)
        : null
        ) 
  }

  alterNotificationSettings(data) {
    data.post_notifications 
    ? (this.setState({
      postsNotification: "checked",
      }))
    : this.setState({ postsNotification: ""})

    data.web_notifications 
    ? (this.setState({
      webNotifications: "checked",
      }))
    : this.setState({ webNotifications: ""})
  }


  post(url, content, callback, callback2) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          if (xhr.responseText) {
            if (xhr.responseText === "response") {
              callback.call();
            } else {
              callback2.call();
            }
          }
        } else {
        }
      }
    };
    xhr.onerror = function(e) {
    };
    xhr.open("POST", url, true);

    xhr.withCredentials = true;
    let file = this.state.selectedFile;
    

    if (file) {
      let qualityValue;
      if (file.size > 9000000) {
        qualityValue = 0.1;
      } else if (file.size > 6000000) {
        qualityValue = 0.2;
      } else if (file.size > 3000000) {
        qualityValue = 0.3;
      } else {
        qualityValue = 0.5
      }
      
  
      new Compressor(file, {
        quality: qualityValue,
        success(result) {
          let formData = new FormData();
    
          // The third parameter is required for server
          formData.append('file', result, result.name);
          // Send the compressed image file to server with XMLHttpRequest.
          xhr.send(
            formData
          )
        },
        error(err) {
          
        },
      });
    }

  }

  fileHandler(event) {
    this.setState({ selectedFile: event.target.files[0] });
    setTimeout(this.handleSubmitFile, 200);
  }

  handleSubmitFile() {
    this.setState({ isLoadingImage: true });
    this.post(
      "https://ventpuff.com/profile/update/avatar",
      {
        selectedFile: this.state.selectedFile
      },
      this.finishUpload
    );
  }

  finishUpload() {
    this.getUserSettingsData();
    this.setState({ selectedFile: null });
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    this.setState({ [name]: event.target.value });
    this.countCharacters();
  }

  handleSubmitProfile() {
    this.setState({ isLoading: true })
    sendPost(
      "https://ventpuff.com/profile/update/profile",
      {
        smokerStatus: this.state.newSmokerStatus,
        quitDate: this.convertTime(), about: this.state.aboutFormValue
      },
      this.getUserSettingsData
    );
  }

  getUserSettingsData() {
   
    const url = "https://ventpuff.com/fetch/settings/profile";

    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => dataset
          ? this.setState({
            userData: dataset,
              isLoading: false,
              isLoadingImage: false,
              aboutFormValue: dataset.about,
              newSmokerStatus: dataset.smokerStatus,
              charactersLeftAbout:  202 - dataset.about.length,
            })
          : this.setState({ userData: [], isLoading: false })
      );
  }

  fetchBlockedUsers() {
    this.setState({ isLoading: true })
    const url = "https://ventpuff.com/fetch/blocked";
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ blockedUsers: dataset, isLoading: false }));
  }

  unblockUser(event) {
    const target = event.target;
    const name = target.name;
    sendPost("https://ventpuff.com/unblock", {username: name}, this.fetchBlockedUsers)
  }

  submitDelete(event) {
    this.setState({ isLoading: true })
    sendPost("https://ventpuff.com/profile/delete", { password: this.state.oldPassword
    }, this.props.loggUser, this.failedDeleting)
    event.preventDefault();
  }

  failedDeleting() {
    this.setState({ passwordWarning: "Incorrect password", isLoading: false})
  }
  


  convertTime() {
    let day = this.state.quitDay
    let month = this.state.quitMonth
    let year = this.state.quitYear
    let quitDate = year.concat(month).concat(day)
    
    this.setState({ newQuitDate: quitDate})
    return quitDate
  }

  countCharacters() {
    let postLength = this.state.aboutFormValue.length
    let charactersCount = 202 - postLength
    this.setState({ charactersLeftAbout: charactersCount })
  }

  


  handleSelect(event) {
    this.setState({newSmokerStatus: event.target.value});
  }

  handleSelectDate(event) {
    
    this.convertTime()
  }

  
  logOut() {
    sendPost("https://ventpuff.com/logout", "", this.props.loggUser);
  }



  render() {
    return (
      <div className="settings-wrapper">
        {this.props.isLogged
        ? <div className="settings-grid">
        <div className="settings-buttons">
           
        <SettingsButton buttonRoute={"account"} buttonName="Edit account" />
       <SettingsButton buttonRoute={"password"} buttonName="Change password" />
       <SettingsButton buttonRoute={"notifications"} buttonName="Notifications" />
       <SettingsButton buttonRoute={"blocked"} buttonName="Blocked users" />
       <SettingsButton buttonRoute={"delete"} buttonName="Delete" />

       <SettingsButton buttonClick={this.logOut} buttonName="Logout" />
        </div>
        {this.props.match.params.settingsType === "account"
        ? <div className="settings-list-wrapper"><SettingsComponent 
        isLoading={this.state.isLoading}
        settingsTitle="Edit account"
        settingsContent={<Account getUserSettingsData={this.getUserSettingsData} user={this.state.user}
        username={this.state.userData.username}
        avatar={`https://ventpuff.com/uploads/${
                this.state.userData.avatar
              }`}
              userData={this.props.userData}
        userRoute={this.state.userRoute}
        isLoadingImage={this.state.isLoadingImage}
        newSmokerStatus={this.state.newSmokerStatus}
        aboutFormValue={this.state.aboutFormValue}
        handleChange={this.handleChange}
        fileHandler={this.fileHandler}
        selectedFile={this.state.selectedFile}
        handleSubmitFile={this.handleSubmitFile}
        handleSubmitProfile={this.handleSubmitProfile} 
        quitDate={this.state.quitDate}
        newQuitDate={this.state.newQuitDate}
        handleSelectDate={this.handleSelectDate} handleSelect={this.handleSelect}
        convertTime={this.convertTime} 
        countCharacters={this.countCharacters}
        charactersLeftAbout={this.state.charactersLeftAbout}
        isLoading={this.state.isLoading}
/>}
       /></div>
        : null
        }
        {this.props.match.params.settingsType === "blocked"
        ? <div className="settings-list-wrapper"><SettingsComponent 
        settingsTitle="Blocked users"
        settingsContent={<BlockedUsersList isLoading={this.state.isLoading}
        fetchBlockedUsers={this.fetchBlockedUsers} blockedUsers={this.state.blockedUsers} unblockUser={this.unblockUser}/>}
        /></div>
        : null
        }
       
        {this.props.match.params.settingsType === "notifications"
        ? <div className="settings-list-wrapper">
        <SettingsComponent 
        settingsTitle="Notifications"
        settingsContent={<Notifications 
          postsNotification={this.state.postsNotification} 
          webNotifications={this.state.webNotifications} changeNotificationSettings={this.changeNotificationSettings}
        fetchNotificationsSettings={this.fetchNotificationsSettings} />}/>
        </div>
        : null
        }
        {this.props.match.params.settingsType === "password"
        ? <div className="settings-list-wrapper">
        <SettingsComponent 
        settingsTitle="Change password"
        settingsContent={<Password 
          handleSubmitPassword={this.handleSubmitPassword}
       handleChangePassword={this.handleChangePassword}
       handleChange={this.handleChange}
       oldPassword={this.state.oldPassword}
       newPassword={this.state.newPassword}
       repeatedPassword={this.state.repeatedPassword}
       warning={this.state.warning}
       oldPasswordValid={this.state.oldPasswordValid} 
       oldPasswordInvalid={this.state.oldPasswordInvalid}        newPasswordValid={this.state.newPasswordValid} 
       newPasswordInvalid={this.state.newPasswordInvalid} 
       oldPasswordWarning={this.state.oldPasswordWarning} 
       newPasswordWarning={this.state.newPasswordWarning} 
       repeatedPasswordWarning={this.state.repeatedPasswordWarning}
       repeatedPasswordValid={this.state.repeatedPasswordValid}
       repeatedPasswordInvalid={this.state.repeatedPasswordInvalid}
       passwordWarningClass={this.state.passwordWarningClass}
       isLoadingPassword={this.state.isLoadingPassword}
       />}
       />
        </div>
        : null
        }
        {this.props.match.params.settingsType === "delete"
        ? <div className="settings-list-wrapper">
        <SettingsComponent 
        settingsTitle="Delete account" 
        settingsContent={<Delete 
        passwordWarning={this.state.passwordWarning}
         submitDelete={this.submitDelete} 
         handleChange={this.handleChange}
         oldPassword={this.state.oldPassword} 
         isLoading={this.state.isLoading} />}
        />
        </div>
        : null
        }
      
        </div>
       :  <div className="settings-grid">
        <div className="settings-buttons">
           
        <SettingsButton buttonRoute={"/register"} buttonName="Register" />
       <SettingsButton buttonRoute={"/login"} buttonName="Login" />
       </div></div>
      }
        </div>        
       

    );
  }
}

export default Settings;
