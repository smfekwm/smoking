import React from "react";
import "./Input.css";
import TextareaAutosize from "react-textarea-autosize";
import Compressor from 'compressorjs';


class Tags extends React.Component {
  render() {
    return (
       <ul className="tag-list">
          <li onClick={this.props.handleClick} className={this.props.ventClass}>vent</li>
          <li onClick={this.props.handleClick} className={this.props.questionClass}>question</li>
          <li onClick={this.props.handleClick} className={this.props.adviceClass}>advice</li>
        </ul>
    );
  }
}

class InputExpanded extends React.Component {

  componentDidMount() {
    document.querySelector(".textarea-title").focus();
  }

  render() {
    return (
   
      <form className="post-input-expanded" onSubmit={this.props.handleSubmit}>
      <TextareaAutosize
          className="textarea-expanded textarea-title"
          minRows={1}
          maxRows={1}
          resize="none"
          name="titleFormValue"
          autoComplete="off"
          maxLength={60}
          placeholder="Add title"
          value={this.props.titleFormValue}
          onChange={this.props.handleChange}
        />
        <br/>
        <TextareaAutosize
          className="textarea-expanded"
          minRows={2}
          maxRows={12}
          resize="none"
          name="textFormValue"
          autoComplete="off"
          maxLength={1400}
          placeholder="Write something..."
          value={this.props.textFormValue}
          onChange={this.props.handleChange}
        />
        <Tags handleClick={this.props.handleClick}
       ventClass={this.props.ventClass}
          questionClass={this.props.questionClass}
          adviceClass={this.props.adviceClass} />
        <div className="input-footer">
       
       <Options
            handleClick={this.props.handleClick}
            fileHandler={this.props.fileHandler}
            selectedFile={this.props.selectedFile}
            handleSubmit={this.props.handleSubmit}
            isPosting={this.props.isPosting}
          />
        </div>
              </form>
    );
  }
}


class Options extends React.Component {
  render() {
    return (
        <ul className="ul-post-options">
 

          <div onChange={this.props.fileHandler} >
            <label htmlFor="file-input">
            <div className="image-upload">

           
            <input onChange={this.props.fileHandler} id="file-input" type="file" />
            </div>

            {this.props.selectedFile
            ? <a className="button-default button-image">...{this.props.selectedFile.name.slice(this.props.selectedFile.name.length - 4, this.props.selectedFile.name.length)}</a>
            :   <a className="button-default button-image">Image</a>
            }
            </label>

          </div>
        
          <div onClick={this.props.handleSubmit}>
          {this.props.isPosting
          ? (
            <a className="button-default button-input">              <i className="fa fa-spinner fa-spin 2x" /></a>

          ) 
          :   <a className="button-default button-input">Send</a>
          
          }
          </div>
        </ul>
    );
  }
}

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      textFormValue: "",
      titleFormValue: "",
      isExpanded: false,
      tagsAreVisible: false,
      tag: null,
      selectedFile: null,
      isPosting: false,
      charactersLeft: 600,
      ventClass: "tag-name",
      questionClass: "tag-name",
      adviceClass: "tag-name"
    };
    this.post = this.post.bind(this);
    this.isPosting = this.isPosting.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.finishPosting = this.finishPosting.bind(this);
    this.expandOptions = this.expandOptions.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.fileHandler = this.fileHandler.bind(this);
    this.hideContextMenu = this.hideContextMenu.bind(this);

  }


  post(url, content, callback, callback2) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function (e) {
     
      if (xhr.readyState === 4) {
       
        if (xhr.status === 200) {
          if (xhr.responseText) {
           if (xhr.responseText === "response") {
             callback.call()
           } else {
            callback2.call()
           }
          }
        } else {
        }
      }
    };
    xhr.onerror = function (e) {
    };
    xhr.open("POST", url, true);
   
    xhr.withCredentials = true;
    let file = this.state.selectedFile

    let textContent = this.state.textFormValue;
    let tagContent = this.state.tag;

    


    if (file) {
      let qualityValue;
      if (file.size > 9000000) {
        qualityValue = 0.1;
      } else if (file.size > 6000000) {
        qualityValue = 0.2;
      } else if (file.size > 3000000) {
        qualityValue = 0.3;
      } else {
        qualityValue = 0.5
      }
      new Compressor(file, {
        quality: qualityValue,
        success(result) {
          let formData = new FormData();
    
          // The third parameter is required for server
          formData.append('file', result, result.name);
          formData.append("post", textContent)
          formData.append("tag", tagContent)
          // Send the compressed image file to server with XMLHttpRequest.
          xhr.send(
            formData
          )
        },
        error(err) {
          
        },
      });
    }
  
    else {
      let formData = new FormData()
      formData.append("title", this.state.titleFormValue);
      formData.append("post", this.state.textFormValue)
      formData.append("tag", this.state.tag)
      formData.append("file", file)
      xhr.send(
        formData
      )
    }

  
  }


  hideContextMenu (callback) {
    
  }
 
  isPosting () {
    this.state.isPosting
    ? this.setState({ isPosting: false })
    : this.setState({ isPosting: true })
  }

  expandOptions() {
    this.state.isExpanded 
    ? this.setState({ isExpanded: false })
    : this.setState({ isExpanded: true })

  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    
    this.setState({ [name]: event.target.value });
    
  }

  handleClick(event) {
    const target = event.target;
    const value = target.textContent;
    value !== "none"
      ? this.setState({ tag: target.textContent })
      : this.setState({ tag: null });
    if (value === "vent") {
      this.setState({ ventClass: "tag-name tag-active", questionClass: "tag-name", adviceClass: "tag-name" })
    } else if (value === "question") {
      this.setState({ ventClass: "tag-name ", questionClass: "tag-name tag-active", adviceClass: "tag-name" })
    } else if (value === "advice") {
      this.setState({ ventClass: "tag-name", questionClass: "tag-name", adviceClass: "tag-name tag-active" })
    } else {
      this.setState({ ventClass: "tag-name", questionClass: "tag-name", adviceClass: "tag-name" })
    }

    if (this.state.tag === value) {
      this.setState({ ventClass: "tag-name", questionClass: "tag-name", adviceClass: "tag-name", tag: null })
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.isPosting();
    this.post(this.props.route, {
      userTitle: this.state.titleFormValue,
      userPost: this.state.textFormValue,
      postId: this.props.postId,
      tag: this.state.tag,
      selectedFile: this.state.selectedFile
    }, this.finishPosting);
   
  }

  finishPosting () {
    this.props.updateData()
    this.isPosting()
    this.setState({ textFormValue: "", selectedFile: null, tag: null, isExpanded: false })

  }

  fileHandler(event) {
    
    this.setState({ selectedFile: event.target.files[0] })
    

  } 

  render() {
    return (
      <div className="post-input">
      {this.state.isExpanded ? (
          <InputExpanded
          hideContextMenu={this.hideContextMenu}
          expandOptions={this.expandOptions}
          textAreaClass={"textarea-expanded"}
          handleSubmit={this.handleSubmit}
          handleChange={this.handleChange}
          titleFormValue={this.props.titleFormValue}
          textFormValue={this.props.textFormValue}
          value={this.state.textFormValue}
          onChange={this.handleChange}
          isPosting={this.state.isPosting}
          fileHandler={this.fileHandler}
          handleClick={this.handleClick}
          tag={this.state.tag}
          selectedFile={this.state.selectedFile}
          ventClass={this.state.ventClass}
          questionClass={this.state.questionClass}
          adviceClass={this.state.adviceClass}
          />
          
        ) :       <form className="post-input-expanded" onSubmit={this.handleSubmit}>
        <TextareaAutosize
          className="textarea-expanded"
          minRows={1}
          maxRows={1}
          resize="none"
          name="titleFormValue"
          autoComplete="off"
          placeholder="Write something..."
          value="Write something..."
          onClick={this.expandOptions}
        />

       

      </form>}

      </div>
    );
  }
}

export default Input;
