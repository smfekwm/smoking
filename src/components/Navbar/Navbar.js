import React from "react";
import "./Navbar.css";
import { NavLink } from "react-router-dom";
import {sendPost} from '../../functions.js';
import { Redirect } from 'react-router';



class Links extends React.Component {
  
  render() {
    const checkActivePosts = (match, location) => {
      const {pathname} = location;

      if (pathname.slice(1,6) !==  "posts") {
        return false
      }
      if (pathname.slice(1,6) === "posts") {
        return true
      }
  }

  const checkActiveSettings = (match, location) => {
    const {pathname} = location;

    if (pathname.slice(1,9) !==  "settings") {
      return false
    }
    if (pathname.slice(1,9) === "settings") {
      return true
    }
}
    return (
        <ul className="ul-navbar">
          <li className="li-navbar">
            <NavLink to="/home" 
            className="navbar-link-button"
            activeClassName="active"
            >
              <i className="fas fa-home navbar-icon" />
              {this.props.isMobile
              ? null
              : <p className="navbar-link-text">Home</p>}            </NavLink>
          </li>
          <li className="li-navbar">
            <NavLink to="/posts/new" 
            className="navbar-link-button"
            isActive={checkActivePosts}
            activeClassName="active"
            >
              <i className="fas fa-edit navbar-icon" />
              {this.props.isMobile
              ? null
              : <p className="navbar-link-text">Feed</p>}            </NavLink>
          </li>
          <li className="li-navbar">
            <NavLink to="/notifications" 
            className="navbar-link-button"
            activeClassName="active"
            >
            {this.props.notificationsCount > 0
            ?  <i className="fas fa-bell navbar-icon"> <div className="new-notifications">
            </div>
            </i>
            :  <i className="fas fa-bell navbar-icon"></i>
            }
            
            {this.props.isMobile
              ? null
              : <p className="navbar-link-text">Notifications</p>}            </NavLink>
          </li>
            <li className="li-navbar" onClick={this.props.showDropdown}>
            <NavLink
              to="/settings/edit"
              className="navbar-link-button"
              isActive={checkActiveSettings}
              activeClassName="active"
            >
              <i className="fas fa-bars navbar-icon" />
              {this.props.isMobile
              ? null
              : <p className="navbar-link-text">Settings</p>}
              
            </NavLink>
           

         
          </li>
          
      
        </ul>
    );
  }
}

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRoute: "aaa",
      dropdownIsVisible: false,
      loggingOut: false,
      homeIsActive: false,
      feedIsActive: false,
      notificationsAreActive: false,
      menuIsActive: false
    };
    this.logOut = this.logOut.bind(this);
    this.loggingOut = this.loggingOut.bind(this);
    this.showDropdown = this.showDropdown.bind(this);
    this.whatRouteIsActive=this.whatRouteIsActive.bind(this);


  }

  whatRouteIsActive () {
    
    //this.setState({ homeIsActive: false,
    //feedIsActive: false,
    //notificationsAreActive: false,
    //menuIsActive: false }) 
  }

  showDropdown() {
    this.state.dropdownIsVisible
    ? this.setState({ dropdownIsVisible: false })
    : this.setState({ dropdownIsVisible: true })
  }

  loggingOut() {
    this.setState({ loggingOut: true})
  }
  
  logOut() {
    sendPost("https://ventpuff.com/logout", "", this.props.loggUser);
    this.loggingOut()
  }


  componentDidMount() {
   

  }
  componentDidUpdate() {

  }
  render() {
   
    
    return (
      <div className={this.props.navbarClass}>
      {this.state.loggingOut 
        ? <Redirect to="/posts/new"/>
        : null
        }
        <Links 
        user={this.props.user}
        isLogged={this.props.isLogged}
        notificationsCount={this.props.notificationsCount}
        showDropdown={this.showDropdown}
        dropdownIsVisible={this.state.dropdownIsVisible} 
        logOut={this.logOut}
        notificationsAreVisible={this.props.notificationsAreVisible}
        showNotifications={this.props.showNotifications}
        openNotification={this.props.openNotification}
        isMobile={this.props.isMobile}
        />
      </div>
    );
  }
}

export default Navbar;
