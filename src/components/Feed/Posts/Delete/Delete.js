import React from "react";
import "./Delete.css";

class Delete extends React.Component {
  render() {
    return (
      <i className="fas fa-trash-alt" onClick={this.props.handleClick}></i>
    )
  }
}

export default Delete;
