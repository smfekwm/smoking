import React from "react";
import "./Like.css";

class Like extends React.Component {
  render() {
    return this.props.liked ? (
      <i className="fas fa-heart red" onClick={this.props.makeLike} />
    ) : (
      <i className="fas fa-heart" onClick={this.props.makeLike} />
    );
  }
}

export default Like;
