import React from "react";
import "./Help.css";


class Countdown extends React.Component {
 
  render() {
    return (
      <div className="help-countdown">
        <p>{this.props.countdownMinutes} : {this.props.countdownSeconds}</p>
      </div>
    )
    
  }
}


class Message extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    
  }
  render() {
    
    return this.props.author === "user"
    ? (<div className="test"> <div className="msg-user">{this.props.text} </div></div>)
    : (<div className="test"><div className="msg-bot">{this.props.text} </div> </div>)
    
  }
}


class Help extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      allData: [],
      countdownMinutes: 6,
      countdownSeconds: 0
    };
    this.startCountDown = this.startCountDown.bind(this);
    this.tick = this.tick.bind(this);

    this.getQuestion = this.getQuestion.bind(this);
    this.userAnswer = this.userAnswer.bind(this);
    this.getAnswer = this.getAnswer.bind(this);
  }

  tick() {
    let minutes = this.state.countdownMinutes
    let seconds = this.state.countdownSeconds
    if (seconds === 0 ) {
      minutes = minutes - 1 
      seconds = 59
    } else {
      seconds = seconds - 1 
    }
    this.setState({ countdownMinutes: minutes, countdownSeconds: seconds })

    if (minutes === 0 & seconds === 0) {
      clearInterval(this.tick, )
    }
  }

  startCountDown() {
    // Start inverval for coutdown every second
    setInterval(this.tick, 1000);
  }

  getQuestion(question) {
    this.setState({ isLoading: true })
    this.setState({ allData: [...this.state.allData, {author: "user", text: question}] })
  }

  userAnswer(answer) {
    this.setState({ allData: [...this.state.allData, {author: "bot", text: answer}], isLoading: false })
  }
  
  getAnswer() {

    const url = "https://ventpuff.com/checkUserMessage";
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(data => this.userAnswer(data.answer));

  }

  componentDidMount() {
    this.props.user === null
    ? this.setState({ allData: [...this.state.allData, {author: "bot", text: "Hello you! How can I help you quit smoking?"}] })
    :  this.setState({ allData: [...this.state.allData, {author: "bot", text: "Hello " + this.props.user+ "! How can I help you  quit smoking?"}] })
  }
  

  render() {
   
    const chatbot = this.state.allData.map(item => (
      <Message
     author={item.author} text={item.text}  />
    ));

    return (
      <div className="help-wrapper">
        <p>Stop your smoking crave</p>
        <a className="settings-submit" onClick={this.startCountDown}>I have crave</a>
        <Countdown countdownMinutes={this.state.countdownMinutes} countdownSeconds={this.state.countdownSeconds} />
        <div className="msg-wrapper">
       
          {chatbot}
         
          {this.state.isLoading ? (
            <div className="load-screen">
              <i className="fa fa-spinner fa-spin" />
            </div>
          ) : null}
          </div>
      
      </div>
    );
  }
}

export default Help;
