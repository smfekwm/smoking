import React from "react";
import "./Profile.css";
import Status from "../Feed/Posts/Status";
import moment from "moment";
import { NavLink } from "react-router-dom";

class Avatar extends React.Component {
  render() {
    return (
      <div className="profile-avatar">
          <div className="image-upload">
            <img alt="profile-avatar" className="profile-avatar" src={this.props.avatar} />
          </div>
      </div>
    );
  }
}

class Header extends React.Component {
  render() {
    return (
      <div className="profile-container">
      <div className="profile-header">
        <Avatar
          avatar={this.props.avatar}
        />
        <div className="profile-user">
        <h3 className="profile-username">{this.props.profileUsername}</h3>
        <div className="profile-status">
          {this.props.smokerStatus ? (
            <Status
              smokerStatus={this.props.smokerStatus}
              quitDate={this.props.quitDate}
            />
          ) : (
            <Status
              smokerStatus={"No status"}
            />
          )}
          </div>
        
       
        </div>
       

        </div>
        {this.props.about
        ?  <p className="profile-about"><b>About: </b>{this.props.about}</p> 
        : null 
        }
       

      </div>
    );
  }
}

class PostsMinimal extends React.Component {
  render() {
    return (
      <NavLink to={this.props.routeToPost} className="post-box-minimal">
        <div className="post-header-minimal">
          <div className="post-date">
          {this.props.timestamp.slice(0, this.props.timestamp.indexOf("ago"))}
          </div>
        </div>
        <div className="post-text-minimal">
          {this.props.title
          ? <b> {this.props.title}<br/></b>
          : null
          }
          {this.props.text.slice(
            0,500
          )}
          {"..."}
        </div>
       
      </NavLink>
    );
  }
}

class Social extends React.Component {
  render() {
    const posts = this.props.data.map(post => (
      <PostsMinimal
        key={`post${post.id}`}
        postId={post.id}
        type="post"
        value="post"
        username={post.username}
        usernameToProfile={`/profile/${post.username}`}
        routeToPost={`/post/${post.id}`}
        avatar={`https://ventpuff.com/uploads/${post.avatar}`}
        title={post.title !== null ?post.title.toUpperCase() 
        : null }        
        text={post.text}
        smokerStatus={post.smokerStatus}
        quitDate={post.quitDate}
        comments={post.comments !== null ? post.comments : []}
        likes={post.postLikes !== null ? post.postLikes : []}
        timestamp={moment(parseInt(post.timestamp, 10))
          .startOf("minutes")
          .fromNow()}
        tag={post.tag}
        image={post.image}
        user={this.props.user}
        isLogged={this.props.isLogged}
        fetchData={this.props.getData}
        updateData={this.props.updateData}
        getDataTag={this.props.getDataTag}
        handleSubmitReport={this.props.handleSubmitReport}
      />
    ));
    return (
        <div className="feed-wrapper-minimal" onScroll={this.props.scrolledToBotom}>{posts}</div>
    );
  }
}


class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: [],
      userData: [],
      userRoute: "",
      showPosts: true
    };
    this.scrolledToBotom = this.scrolledToBotom.bind(this);
  }

  getData(user) {
    this.setState({ isLoading: true });
    const url = `https://ventpuff.com/fetch/profile/${user}`;

    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset =>
        dataset.userData
          ? this.setState({
              data: dataset.data,
              userData: dataset.userData,
              isLoading: false,
            })
          : this.setState({ userData: null })
      );
  }

  getMoreData(user) {
    
    this.setState({ isLoading: true });
    const url = `https://ventpuff.com/scroll/profile/${user}`;

    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset =>
        dataset.userData
          ? this.setState({
              data: dataset.data,
              userData: dataset.userData,
              isLoading: false,
            })
          : this.setState({ userData: null })
      );
  }


  scrolledToBotom() {
    let profileFeed;
    let userRoute = this.props.match.params.userProfile;
    this.props.isMobile
    ? profileFeed = document.querySelector(".feed")
    : profileFeed = document.documentElement
    
      if (Math.round((profileFeed.scrollTop + profileFeed.clientHeight +2.5)) === profileFeed.scrollHeight) {
        this.getMoreData(userRoute);
        
      
    }
    
    
    }
 
  debounce(func, wait = 1000, immediate) {
    let timeout;
    return function() {
      let context = this,
        args = arguments;
      clearTimeout(timeout);
      timeout = setTimeout(function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      }, wait);
      if (immediate && !timeout) func.apply(context, args);
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrolledToBotom)
    let userRoute = this.props.match.params.userProfile;
    this.setState({ userRoute: userRoute });
    this.getData(userRoute);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrolledToBotom)

  }

  render() {
    return (
      <div>
        {this.state.userData ? (
          <div className="profile-wrapper" >
            <Header
              profileUsername={this.state.userData.username}
              avatar={`https://ventpuff.com/uploads/${
                this.state.userData.avatar
              }`}
              user={this.props.user}
              userRoute={this.state.userRoute}
              smokerStatus={this.state.userData.smokerStatus}     
              about={this.state.userData.about}       />

            <Social onScroll={this.scrolledToBotom} data={this.state.data} scrolledToBotom={this.scrolledToBotom} />
          </div>
        ) : (
          <div className="profile-wrapper">
            <h3>No user</h3>
          </div>
        )}
      </div>
    );
  }
}

export default Profile;
