import React from "react";
import "./Edit.css";

class Edit extends React.Component {
  render() {
    return (
      <i className="fas fa-edit" onClick={this.props.handleClick} />
    );
  }
}

export default Edit;
