import React from "react";
import { Redirect } from 'react-router';
import "./Login.css";


class Warning extends React.Component {
  render() {
    return <p className="warning-text"> {this.props.warning}</p>;
  }
}

class LoginForm extends React.Component {
  render() {
    return (
     <div className="register-box">
        <div className="form-float" >
      <div className="input-float">
          <input required
            className="input"
            name="username"
            type="text"
            value={this.props.username}
            onChange={this.props.handleChange}
          />
          <span className="label">Username</span>
          {this.props.usernameWarning
          ? <Warning warning={this.props.usernameWarning} />
          : null 
          }
        </div>
        <div className="input-float">
          <input required
            className="input"
            name="password"
            type="password"
            value={this.props.password}
            onChange={this.props.handleChange}
          />
          <span className="label">Password</span>
          {this.props.passwordWarning
          ? <Warning warning={this.props.passwordWarning} />
          : null 
          }
        </div>
        <div className="settings-footer">
        {this.props.isLoading ? (
          <a className="button-default hidden-text">Login
            <i className="fa fa-spinner fa-spin inside" />
          </a>
        ) : (
          <a className="button-default" onClick={this.props.handleSubmit}>
            Login
          </a>
        )}
        </div>
        </div>
        </div>

    );
  }
}

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      failedAuthentication: false,
      usernameWarning: "",
      passwordWarning: ""
    };
    this.post = this.post.bind(this);
    this.failedAuthentication = this.failedAuthentication.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    this.setState({ [name]: event.target.value });
  }


  handleSubmit(event) {
    this.setState({ isLoading: true })
    this.post("https://ventpuff.com/login", {
      username: this.state.username,
      password: this.state.password
    }, this.props.loggUser, this.failedAuthentication)
    event.preventDefault();
  }

  failedAuthentication(text) {
    this.setState({ usernameWarning: "", passwordWarning: "" })
    if (text === "username") {
      this.setState({ failedAuthentication: true, usernameWarning: "Username not registered" })
    } else {
      this.setState({ failedAuthentication: true, passwordWarning: "Incorrect password" })
    }
    this.setState({ isLoading: false })
  }



  post(url, content, callback, callback2) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function (e) {
     
      if (xhr.readyState === 4) {
       
        if (xhr.status === 200) {
         
          if (xhr.responseText) {
           if (xhr.responseText === "response") {
             callback.call()
           } else {
          callback2(xhr.responseText)
           }
          }
        } else {
          console.error(xhr.statusText);
        }
      }
    };
    xhr.onerror = function (e) {
      console.error(xhr.statusText);
    };
    xhr.open("POST", url, true);
   
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.withCredentials = true;
   
    xhr.send(
      JSON.stringify({
        content
      })
    )
  }
  
  render() {
   
    return (
      <div className="register-wrapper">
      <h2 className="top-title">Login</h2>

        {this.props.isLogged ? (
          
          <Redirect to="/posts/new"/>
        ) : (
          <LoginForm loggUser={this.props.loggUser}
          isLoading={this.state.isLoading}
          username={this.state.username}
          password={this.state.password}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          failedAuthentication={this.state.failedAuthentication} 
          usernameWarning={this.state.usernameWarning}  passwordWarning={this.state.passwordWarning}/>
        )}
      </div>
    );
  }
}

export default Login;
