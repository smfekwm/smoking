WITH posting AS (
        SELECT users.smokerstatus AS "smokerStatus", users.quitdate AS "quitDate", posts.id AS "postsId",posts.username AS "postAuthor", posts.text AS "postText", posts.timestamp AS "postTimestamp" FROM users 
        GROUP BY posts.id
     ), commenting AS (
        SELECT region
        FROM regional_sales
        WHERE total_sales > (SELECT SUM(total_sales)/10 FROM regional_sales)
     )
SELECT region,
       product,
       SUM(quantity) AS product_units,
       SUM(amount) AS product_sales
FROM orders
WHERE region IN (SELECT region FROM top_regions)
GROUP BY region, product;
"WITH dataset AS (SELECT )


SELECT json_build_object('id', p.id, 'text', p.text, 'comments',
    (SELECT json_agg(json_build_object('id', c.parrent, 'text', c.text))
     FROM comments c WHERE p.id = c.parrent)) json
FROM posts p;

And then your pg-promise example would be:

const query =
    `SELECT json_build_object('id', q.id, 'content', q.content, 'votes',
        (SELECT json_agg(json_build_object('id', v.id, 'value', v.value))
         FROM votes v WHERE q.id = v.question_id)) json
    FROM questions q`;

db.map(query, [], a => a.json)
    .then(data => {
        
    })
    .catch(error => {
        
    });

con.task(t => {
    const a = posts => t.any('SELECT * FROM comments WHERE posts.id = comments.parrent )
        .then(comment => {
            comments.comment = comments;
            return post;
        });
    return t.map('SELECT *, avatar FROM post, users WHERE user = $1 AND user = alias ORDER BY time DESC LIMIT 10 OFFSET $2', [username, pos], a)
        .then(t.batch);
})
    .then(posts => {
        res.send(posts);
    })
    .catch(error => {
        
    });



db.task(t => {
    return t.any('SELECT users.username, posts.username FROM users, posts WHERE users.username = posts.username')
        .then(user => {
            if(user) {
                return t.any('SELECT * from comments, posts WHERE comments.parrent = posts.id);
            }
            return []; // user not found, so no events
        });
})
    .then(events => {
        // success
    })
    .catch(error => {
        // error
    });

    con.task(async t => {
    const data = await t.any('SELECT *, posts.text FROM posts, users WHERE posts.username = users.username ORDER BY posts.id DESC LIMIT 10);
    const a = posts => ({query: 'SELECT * FROM comments WHERE posts.id = comments.parrent',});
    const queries = pgp.helpers.concat(data.map(a));
    await t.multi(queries)
        .then(comment => {
            data.forEach((p, index) => {
                p.comment = comment[index];
            });
        });
    return data;
})
    .then(data => {
        res.send(data);
    })
    .catch(error => {
        
    });




    db.task(t => {
    return t.any('SELECT users.username, posts.username FROM users, posts WHERE users.username = posts.username')
        .then(user => {
            if(user) {
               
            }
            return []; // user not found, so no events
        });
})
    .then(events => {
        // success
    })
    .catch(error => {
        // error
    });



db.task(t => {
    return t.any('select * from users,posts where users.username = posts.username)
        .then(results => {
            return t.batch([
                t.one('select posts.username as author, posts.text, posts.timestamp as post, users.lastlogin, users.smokerstatus as user from orden as o inner join estado as e on o.idestado = e.gid inner join grupo as g on o.idgrupo = g.id inner join via as v on o.idctra = v.gid and o.gid = $1', resultado.gid),
                t.any('select * from operacion order by gid where idgrupo = $1', resultado.idgrupo),
                t.any('select m.gid, m.fechamed as fecha, m.cantidad, m.costemed as coste, o.codigo, o.descr from medicion m, operacion o where m.idorden = $1 and m.idope = o.gid order by fecha asc', resultado.gid)
            ]);
        });
})
    .then(data => {
        res.render('mediciones/nuevaMed', {
            title: 'Crear / Modificar parte de trabajo',
            orden: data[0],
            operaciones: data[1],
            medicion: [],
            mediciones: data[2],
            errors: []
        });
    })
    .catch(next);


    function getUsers(t) {
    return t.map('SELECT * FROM users, posts WHERE users.username = posts.username, user => {
        return t.map('SELECT * FROM posts, users WHERE users.username = posts.username, user.id, post=> {
            return t.any('SELECT * FROM comments, posts WHERE comments.parrent = posts.id, post.id)
                .then(comment=> {
                    post.comment = post;
                    return post;
                });
        })
            .then(t.batch) // settles array of requests for Cars (for each event)
            .then(post=> {
                user.post = post;
                return user;
            });
    }).then(t.batch); // settles array of requests for Events (for each user)
}