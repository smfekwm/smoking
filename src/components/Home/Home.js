import React from "react";
import "./Home.css";
import { NavLink } from "react-router-dom";



class Title extends React.Component {
  render() {
    return (
      <div className="title-container">
      <h1 className="main-title">Stop smoking...</h1>
      <h3 className="">...or at least talk about how hard are you trying, ask for help, advice others or just vent your frustrations. </h3>
      <h2>But please, stay polite.</h2>
      <br/>
      <p>You can just read other's stories, but if you want to post something, you have to <NavLink className="basic-link" to="/register">register</NavLink>.<br/><br/>We don't need your real name or your email for better anonymity, but it comes with a cost - if you ever forget your password, you will loose your account forever.</p>
      </div>
    )
  }
}


class ChangeLog extends React.Component {
  render() {
    return (
      <ul className="changelog">
        <h4>Changelog:</h4>
      <li className="li-changelog">
        <div className="changelog-header">
        <p className="changelog-name">v0.1 - First set up</p>
        <p className="changelog-time">5. 12. 18</p>
        </div>
        <p className="changelog-content">Authentication, posting and basic notifications functionality.</p>
        </li>
      </ul>
    )
  }
}

class Footer extends React.Component {
  render() {
    return (
      <div className="footer-wrapper">
        <div className="footer">
      <p>Any requests, ideas or bugs please email here:</p>
      <p>contact@lastpuff.com</p>
      </div>
      </div>
    )
  }
}




class Home extends React.Component {
  render() {

    return (
      <div className="intro-wrapper" >
        <div className="intro-body">
        <Title/>
        <ChangeLog />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
