import React from "react";
import "./Loading.css";


class Loading extends React.Component {
  render() {
    return (
    
        <div className="load-screen">
          <i className="fa fa-spinner fa-spin" />
        </div>
     
    )
  }
}

export default Loading;
