import React from "react";
import "./Comments.css";
import moment from "moment";
import BasicInput from "../Comments/Input/BasicInput";
import Options from "../Options/Options"
import Report from "../Report/Report"
import {sendPost} from '../../../../functions.js';
import { NavLink } from "react-router-dom";
import Like from "../Like/Like";



class SubComment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    isEdited: false,
    showReport: false,
    textFormValueReport: "",
    reportType: "",
    optionsAreVisible: false,

  };
  this.showReport = this.showReport.bind(this);
  this.handleChange = this.handleChange.bind(this);
  this.handleSubmitReport = this.handleSubmitReport.bind(this);
  this.makeLike = this.makeLike.bind(this);
  this.blockUser = this.blockUser.bind(this);
  this.showOptions = this.showOptions.bind(this);
  this.hideContextMenu = this.hideContextMenu.bind(this);

}
showOptions() {
  this.state.optionsAreVisible 
  ? this.setState({ optionsAreVisible: false, deleteDialogIsVisible: false })
  : this.setState({ optionsAreVisible: true })
}

hideContextMenu (callback) {
  document.querySelector(".post-options-menu-wrapper").addEventListener("click", function(event) {
  if (event.target !== this) {
    return
  }
  callback.call();
  })
}



blockUser() {
  let username = this.props.subCommentUsername
  sendPost("https://ventpuff.com/block", { username: username}, this.props.loadNewData);
}

makeLike() {
  sendPost("https://ventpuff.com/like/subcomment", { subCommentId: this.props.subCommentId }, this.props.updateData);
}

handleSubmitReport(event) {
  event.preventDefault();
  let contentType = "subComment"
  let id = this.props.subCommentId
  let username = this.props.subCommentUsername
  let text = this.props.subCommentText
  let report = this.state.textFormValueReport
  let reportType = this.state.reportType
  
  sendPost("https://ventpuff.com/report", { contentType: contentType, id: id, username: username, text: text, report: report, reportType: reportType });
}

handleChange(event) {
  const target = event.target;
  const name = target.name;
  
  this.setState({ [name]: event.target.value });
  
}
showReport() {
  this.state.showReport
    ? this.setState({ showReport: false})
    : this.setState({ showReport: true, showOptions: false });
}

  render() {
    return (
      <div className="comments-wrapper">
      <div className="vertical-line"></div>
      <div className="comments-box sub-comment">
     
      {this.state.showReport
             ? <Report 
             handleSubmitReport={this.handleSubmitReport}
             handleChange={this.handleChange}
             textFormValueReport={this.textFormValueReport}
             showReport={this.showReport}
             />
             : null
             }
      <div className="comment-header">
        
          <div className="comment-user">
          <NavLink className="post-username link-big" to={this.props.usernameToProfile}>
            {this.props.subCommentUsername}
            </NavLink>
          </div>
      
          <div className="comment-menu">
          {this.props.user & this.props.user !== this.props.subCommentUsername
          ?  <i className="fas fa-ellipsis-h post-options-comment float-right" onClick={this.showOptions}></i>
             : null}
             
             <div className="float-right">
          {this.props.likes ? (
              <div className="post-likes-text float-right">{this.props.likes.length}</div>
            ) : null}
            {this.props.user !== null ? (
              <div className="post-heart float-right">
                <Like
                makeLike={this.makeLike.bind(this)}
                  liked={this.props.liked}
                />
              </div>
            ) : (
              <div className="post-heart float-right">
                <Like/>
              </div>
            )}
            
          </div>
              </div>
        </div>

        <CommentsText commentText={this.props.subCommentText} />
      </div>
      </div>
    );
  }
}

class CommentsText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLong: false,
      isExpanded: false
    };
    this.isExpanded = this.isExpanded.bind(this);
  }

  isLong() {
    this.props.commentText.length > 200
      ? this.setState({ isLong: true })
      : this.setState({ isLong: false });
  }

  isExpanded() {
    this.state.isExpanded
      ? this.setState({ isExpanded: false, isLong: true })
      : this.setState({ isExpanded: true, isLong: false });
  }

  componentDidMount() {
    this.isLong();
  }

  render() {
    return (
      <div className="comment-text">
        {this.state.isLong ? (
          <div>
            {this.props.commentText.slice(
              0,
              this.props.commentText.slice(0, 200).lastIndexOf(" ")
            )}
            {"... "}
            <div className="show-more" onClick={this.isExpanded}>
              (more)
            </div>
          </div>
        ) : (
          this.props.commentText
        )}
      </div>
    );
  }
}

class Comments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSubComments: false,
      showOptions: false,
      isEdited: false,
      showReport: false,
      textFormValueReport: "",
      reportType: "",
      optionsAreVisible: false,

    };
    this.showSubComments = this.showSubComments.bind(this);
    this.showReport = this.showReport.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmitReport = this.handleSubmitReport.bind(this);
    this.blockUser = this.blockUser.bind(this);
    this.showOptions = this.showOptions.bind(this);
    this.hideContextMenu = this.hideContextMenu.bind(this);
    this.reportResponse = this.reportResponse.bind(this);
    this.makeLike = this.makeLike.bind(this);

  }

  makeLike() {
    sendPost("https://ventpuff.com/like/comment", { commentId: this.props.commentId }, this.props.updateData);
  }

  showSubComments() {
    this.state.showSubComments
      ? this.setState({
          showSubComments: false
        })
      : this.setState({ showSubComments: true });
  }

  handleSubmitReport(event) {
    event.preventDefault();
    let contentType = "post"
    let id = this.props.postId
    let username = this.props.username
    let text = this.props.text
    let report = this.state.textFormValueReport
    let reportType = this.state.reportType
    sendPost("https://ventpuff.com/report", { contentType: contentType, id: id, username: username, text: text, report: report, reportType: reportType }, this.reportResponse);
  }

  blockUser() {
    let username = this.props.commentUsername
    sendPost("https://ventpuff.com/block", { username: username}, this.props.loadNewData);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    
    this.setState({ [name]: event.target.value });
    
  }

  hideContextMenu (callback) {
    document.querySelector(".post-options-menu-wrapper").addEventListener("click", function(event) {
    if (event.target !== this) {
      return
    }
    callback.call();
    })
  }

  reportResponse() {
    this.setState({ reportWasSent: true })
  }

  showReport() {
    this.state.showReport
      ? this.setState({ showReport: false})
      : this.setState({ showReport: true, optionsAreVisible: false });
  }


  showOptions() {
    this.state.optionsAreVisible 
    ? this.setState({ optionsAreVisible: false, deleteDialogIsVisible: false })
    : this.setState({ optionsAreVisible: true })
  }


  render() {
    const subComments = this.props.subComments.map(subComment => (
      <SubComment
        key={subComment.id}
        subCommentId={subComment.id}
        subCommentText={subComment.text}
        subCommentUsername={subComment.username}
        usernameToProfile={`/profile/${subComment.username}`}
        subCommentDate={moment(parseInt(subComment.timestamp, 10))
          .startOf("minutes")
          .fromNow()}
          likes={subComment.subCommentLikes}
        liked={subComment.subCommentLikes !== null ?
        ((subComment.subCommentLikes.map(like => like.username)).includes(this.props.user)
        ? true
        : false
        )
        : false   
        }
        user={this.props.user}
        updateData={this.props.updateData}
        loadNewData={this.props.loadNewData}
      />
    ));
   
    return (
 <div className="comments-wrapper">      
   <div className="vertical-line"></div>
      <div className="comments-box">
      {this.state.showReport
             ? <Report 
             handleSubmitReport={this.handleSubmitReport}
             handleChange={this.handleChange}
             textFormValueReport={this.props.textFormValueReport}
             showReport={this.showReport}
             />
             : null
             }
      <div className="comment-header">
         
          <div className="comment-user">
          <NavLink className="post-username link-big" to={this.props.usernameToProfile}>
            {this.props.commentUsername}
            </NavLink>
          </div>
      
          <div className="comment-menu">
          {this.props.user & this.props.user !== this.props.commentUsername
          ?  <i className="fas fa-ellipsis-h post-options-comment float-right" onClick={this.showOptions}></i>
             : null}
        
          <div className="comment-list float-left" onClick={this.showSubComments}>
            <i className="far fa-comment float-right">
              {"  "}
              {this.props.subComments.length > 0
                ? this.props.subComments.length
                : null}
            </i>
          </div>
          <div className="float-right">
          {this.props.likes ? (
              <div className="post-likes-text float-right">{this.props.likes.length}</div>
            ) : null}
            {this.props.user !== null ? (
              <div className="post-heart float-right">
                <Like
                makeLike={this.makeLike.bind(this)}
                  liked={this.props.liked}
                />
              </div>
            ) : (
              <div className="post-heart float-right">
                <Like/>
              </div>
            )}
            
          </div>
              </div>
        </div>


        <CommentsText commentText={this.props.commentText} />


        {this.state.showSubComments ? (
          this.props.subComments === null ? (
            <div className="input-box">
              <BasicInput
                route="https://ventpuff.com/post/subcomment"
                isLogged={this.props.isLogged}
                updateData={this.props.updateData}
                postId={this.props.commentId}
                user={this.props.user}
                textAreaClass={"textarea-post"}
              />
            </div>
          ) : (
            <div>
              <div className="input-box">
                <BasicInput
                  route="https://ventpuff.com/post/subcomment"
                  isLogged={this.props.isLogged}
                  updateData={this.props.updateData}
                  postId={this.props.commentId}
                  user={this.props.user}
                  textAreaClass={"textarea-post"}
                />
              </div>
              {subComments}
            </div>
          )
        ) : null}
      </div>
      </div>
    );
  }
}

export default Comments;
