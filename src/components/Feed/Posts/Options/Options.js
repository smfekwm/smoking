import React from "react";

class Options extends React.Component {
  componentDidMount() {
    this.props.hideContextMenu(this.props.showOptions);
  }

  render() {
    return (
      <div className="post-options-menu-wrapper">
        {this.props.deleteDialogIsVisible ? (
          <div className="post-options-menu">
          <div className="ul-post-edit">
              <p>Do you want to delete your post?</p>
              <div className="buttons-post-box">

              <a
                className="button-default warning-red"
                onClick={this.props.postDelete}
              >
                Delete
              </a>
              <a className="button-default" onClick={this.props.showOptions}>
                Cancel
              </a>
              </div>
          </div>
          </div>

        ) : (
          <div className="post-options-menu">
            {this.props.user === this.props.username ? (
              <div className="ul-post-edit">
               <div> <p onClick={this.props.isEdited} className="li-post-edit">
                  Edit
                </p>
                <p onClick={this.props.confirmDelete} className="li-post-edit">
                  Delete
                </p>
                <p onClick={this.props.showOptions} className="li-post-edit">
                  Cancel
                </p>              </div>


                

              </div>
            ) : (
              <div className="ul-post-edit">
                {" "}
                <p className="li-post-edit" onClick={this.props.showReport}>
                  Report
                </p>
                <p onClick={this.props.blockUser} className="li-post-edit">
                  Block user
                </p>
                <p onClick={this.props.showOptions} className="li-post-edit">
                  Cancel
                </p>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default Options;
