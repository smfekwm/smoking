import React from "react";
import "./Edit.css";
import TextareaAutosize from 'react-textarea-autosize';


function post(url, content) {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.withCredentials = true;
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({
      content
}));

}

class EditForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textFormValue: this.props.postText,
  };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    this.setState({[name]: event.target.value});
  }

  handleSubmit(event) {
    setTimeout(this.props.isEdited, 300)
    post("https://ventpuff.com/edit", {postId: this.props.postId, text: this.state.textFormValue})
    event.preventDefault();
    //TODO - change to only fetch old data and not new
    setTimeout(this.props.updateData, 300);
  }

  componentDidMount() {
    document.querySelector(".textarea-edit").focus();
  }

    render() {
      return (
        <div>
          <TextareaAutosize
          className = "textarea-edit"
          minRows={2}
          maxRows={10}
          resize="none"
          onChange={e => this.setState({textFormValue: e.target.value})}
          defaultValue={this.props.postText} />
          <div className="buttons-post-box">
          <a className="button-post" onClick={this.handleSubmit}>Save</a>
          <a className="button-post" onClick={this.props.isEdited}>Cancel</a>
          </div>
          </div>
          
      );
    }
  }

export default EditForm;


