import React from "react";
import "./Feed.css";
import Posts from "./Posts/Posts";
import Input from "./Posts/Comments/Input/Input";
import Sign from "./Posts/Sign/Sign";
import { NavLink } from "react-router-dom";
import moment from "moment";








class Switcher extends React.Component {
  
  render() {
   
    return (
        <ul className="ul-switcher">
          <NavLink className="li-switcher link-big" 
          activeClassName="switcher-active"
          to="/posts/new">New</NavLink>
          <NavLink className="li-switcher link-big"             activeClassName="switcher-active"
          to="/posts/top">Top</NavLink>
        </ul>
    )
  }
}

class EmptyPost extends React.Component {
  
  render() {
   
    return (
      <div className="post-box">
       <div className="post-header">
         <div className="post-username username-empty"
         ></div>
         <div className="post-date date-empty"></div>
       </div>
       <div className="post-text text-empty"></div>
       <div className="post-text text-empty"></div>
       <div className="post-text text-empty"></div>
      </div>
    )
  }
}




class Social extends React.Component {

  render() {
    const posts = this.props.data.map(post => (
      <Posts
        key={`post${post.id}`}
        postId={post.id}
        type="post"
        value="post"
        username={post.username}
        usernameToProfile={`/profile/${post.username}`}
        avatar={`https://ventpuff.com/uploads/${post.avatar}`}
        title={post.title !== null ?post.title.toUpperCase() 
        : null }
        text={post.text}
        smokerStatus={post.smokerStatus}
        quitDate={post.quitDate}
        comments={post.comments !== null ?post.comments 
        : [] }
        likes={post.postLikes}
        liked={post.postLikes !== null ?
        ((post.postLikes.map(like => like.username)).includes(this.props.user)
        ? true
        : false
        )
        : false   
        }
        timestamp={moment(parseInt(post.timestamp, 10))
              .startOf("minutes")
              .fromNow()}
        tag={post.tag}
        image={post.image}
        user={this.props.user}
        isLogged={this.props.isLogged}
        fetchData={this.props.loadMoreData}
        updateData={this.props.updateData}
        getDataTag={this.props.getDataTag}
        handleSubmitReport={this.props.handleSubmitReport}
        loadNewData={this.props.loadNewData}
      />
    ));
    return (
      
        <div className="feed-wrapper" onScroll={this.props.scrolledToBotom}>
          {this.props.isLogged
          ? <Input 
          user={this.props.user}
          isLogged={this.props.isLogged}
          updateData={this.props.updateData}
          setLoading={this.props.setLoading}
          isLoading={this.props.isLoading}
          route="https://ventpuff.com/send/post" 
          textAreaClass={"textarea-post main-post"}/>
          : <Sign />
          }
         
      
          {posts}
          {this.props.isLoading ? (
            <div className="loading-box">
              <i className=" fa fa-spinner fa-spin loading-posts" />
             
            </div> 
          ) : null}
          {this.props.dataLength === this.props.data.length 
          ? 
            <div className="post-box-end">No new content</div>
          : null}
        </div>

    )

  }
}



class Feed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      scrolledToBotom: false,
      isLoading: false,
      dataLength: null,
      postNew: true,
      emptyFeed: true,
    };
    this.scrolledToBotom = this.scrolledToBotom.bind(this);
    this.loadNewData = this.loadNewData.bind(this);
    this.loadMoreData = this.loadMoreData.bind(this);
    this.updateData = this.updateData.bind(this);
    this.getDataTag = this.getDataTag.bind(this);
    this.test = this.test.bind(this);

  }




  scrolledToBotom() {
    let feed = document.documentElement;
  
    if (Math.round((feed.scrollTop + feed.clientHeight )) >= feed.scrollHeight - 15 ) {
      {this.state.dataLength === this.state.data.length
        ? null
        : this.loadMoreData()
        }
      }
     
     /*
           let sumScroll = feed.scrollTop + feed.clientHeight
      (sumScroll + " :: " + feed.scrollHeight )
    
      let feed = document.querySelector(".wrapper")
     var scrollY = feed.scrollHeight - feed.scrollTop;
     var height = feed.offsetHeight;
     var offset = height - scrollY;
     
     if (offset == 0 || offset == 1) {
     // this.loadMoreData()
         // load more content here
     }
  debounce(func, wait = 1000, immediate) {
    let timeout;
    return function() {
      let context = this,
        args = arguments;
      clearTimeout(timeout);
      timeout = setTimeout(function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      }, wait);
      if (immediate && !timeout) func.apply(context, args);
    };
  }

    */
    }
   



  loadNewData() {
    this.test();

    let postType = this.props.match.params.type

    const url = `https://ventpuff.com/posts/${postType}`;
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ data: dataset, emptyFeed: false }));
  }

  loadMoreData() {
    
    this.setState({ isFetching: true })
    let postType = this.props.match.params.type
    this.setState({ isLoading: true });



    const url = `https://ventpuff.com/fetch/posts/${postType}`;
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ data: this.state.data.concat(dataset), isLoading: false }));
  }

  updateData() {
    const url = "https://ventpuff.com/fetch/update";
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ data: dataset }));
  }


  getDataTag(event) {
    let targetTag = event.target.textContent.slice(1,);
    
    this.setState({ isLoading: true });
    let url = `https://ventpuff.com/posts/${targetTag}`;
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ data: dataset, isLoading: false }));
  }





  test() {
    const url = "https://ventpuff.com/length";
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(data => this.setState({ dataLength: data.length}) )
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrolledToBotom)
    this.loadNewData()

}
componentWillUnmount () {
  window.removeEventListener("scroll", this.scrolledToBotom, false);

}

  render() {
    

    return (
        <div className="wrapper" onScroll={this.scrolledToBotom}>
       <Switcher/>
          
       {this.props.emptyFeed
          ? <div className="feed-wrapper">
            <EmptyPost />
            <EmptyPost />
            <EmptyPost />
            <EmptyPost />
            <EmptyPost />
            <EmptyPost />
            <EmptyPost />
            <EmptyPost />
            <EmptyPost />
            </div>
          :  <Social data={this.state.data} user={this.props.user} isLogged={this.props.isLogged} loadMoreData={this.loadMoreData} 
        fetchData={this.fetchData}
        updateData={this.updateData}
        scrolledToBotom={this.scrolledToBotom} isLoading={this.state.isLoading} dataLength={this.state.dataLength} 
        getDataTag={this.getDataTag} 
        emptyFeed={this.state.emptyFeed}
        handleSubmitReport={this.handleSubmitReport}
        loadNewData={this.loadNewData} />
          }
       
        
        


       
        </div>

    );
  }
}

export default Feed;
