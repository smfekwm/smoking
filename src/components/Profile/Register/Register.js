import React from "react";
import "./Register.css";
import { Redirect } from "react-router";
import { sendPost } from "../../../functions.js";
import { NavLink } from "react-router-dom";

class Policy extends React.Component {
  render() {
    return (
      <div className="register-box">
        <p className="register-box-small">
          By signing up, you agree to our{" "}
          <NavLink className="basic-link" to="/terms">
            Terms of use and Privacy Policy
          </NavLink>
          .<br/>
        </p>
      </div>
    );
  }
}

class Warning extends React.Component {
  render() {
    const warningClass = `warning-text ${this.props.warningClass}`;
    return <p className={warningClass}> {this.props.warning}</p>;
  }
}

class RegisterForm extends React.Component {
  render() {
    return (
      <div className="register-box">
        <div className="form-float">
          <div className="input-float">
            <input
              required
              className="input input-username"
              name="username"
              type="text"
              autoComplete="off"
              value={this.props.username}
              onChange={this.props.handleChangeUsername}
            />
            <span className="label">Username</span>
            {this.props.usernameWarning ? (
              <Warning
                warning={this.props.usernameWarning}
                warningClass="invalid"
              />
            ) : null}
            <div className="input-icon">
              {this.props.isLoadingUsername ? (
                <i className="fa fa-spinner fa-spin" />
              ) : null}
              {this.props.usernameValid &
              (this.props.isLoadingUsername === false) ? (
                <i className="fas fa-check-circle valid" />
              ) : null}
              {this.props.usernameInvalid &
              (this.props.isLoadingUsername === false) ? (
                <i className="fas fa-times-circle invalid" />
              ) : null}
            </div>
          </div>
          <div className="input-float">
            <input
              required
              className="input input-password"
              name="password"
              type="password"
              value={this.props.password}
              onChange={this.props.handleChangePassword}
            />
            <span className="label">Password</span>
            {this.props.passwordWarning ? (
              <Warning
                warning={this.props.passwordWarning}
                warningClass="invalid"
              />
            ) : null}
            <div className="input-icon">
              {this.props.passwordValid ? (
                <i className="fas fa-check-circle valid" />
              ) : null}
              {this.props.passwordInvalid ? (
                <i className="fas fa-times-circle invalid" />
              ) : null}
            </div>
          </div>
          <div className="input-float">
            <input
              required
              className="input input-password"
              name="repeatedPassword"
              type="password"
              value={this.props.repeatedPassword}
              onChange={this.props.handleChangePassword}
            />
            <span className="label">Repeat password</span>
            {this.props.repeatedPasswordWarning ? (
              <Warning
                warning={this.props.repeatedPasswordWarning}
                warningClass="invalid"
              />
            ) : null}
            <div className="input-icon">
              {this.props.repeatedPasswordValid ? (
                <i className="fas fa-check-circle valid" />
              ) : null}
              {this.props.repeatedPasswordInvalid ? (
                <i className="fas fa-times-circle invalid" />
              ) : null}
            </div>
          </div>
          <div className="input-float">
            <input
              required
              className="input input-answer"
              name="answer"
              type="text"
              autoComplete="off"
              value={this.props.answer}
              onChange={this.props.handleChangeAnswer}
            />
            <span className="label">
              Write answer {this.props.randomNumber} x 2 ={" "}
            </span>
            {this.props.answerWarning ? (
              <Warning
                warning={this.props.answerWarning}
                warningClass="invalid"
              />
            ) : null}
            <div className="input-icon">
              {this.props.answerValid ? (
                <i className="fas fa-check-circle valid" />
              ) : null}
              {this.props.answerInvalid ? (
                <i className="fas fa-times-circle invalid" />
              ) : null}
            </div>
          </div>

          <div className="settings-footer">
            {this.props.isLoading ? (
              <a className="button-default hidden-text">
                Register
                <i className="fa fa-spinner fa-spin inside" />
              </a>
            ) : (
              <a className="button-default" onClick={this.props.registerUser}>
                Register
              </a>
            )}
            {this.props.registerWarning ? (
              <Warning
                warning={this.props.registerWarning}
                warningClass={this.props.registerClass}
              />
            ) : (
              <Warning warning={" "} warningClass={this.props.registerClass} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      repeatedPassword: "",
      warning: "",
      usernameValid: false,
      usernameInvalid: false,
      usernameWarning: "",
      passwordValid: false,
      passwordInvalid: false,
      passwordWarning: false,
      repeatedPasswordValid: false,
      repeatedPasswordInvalid: false,
      repeatedPasswordWarning: "",
      answerValid: false,
      answerInvalid: false,
      answerWarning: false,
      registerWarning: "",
      registerClass: "",
      randomNumber: "",
      answer: "",
      isSpam: true,
      isLoading: false,
      isLoadingUsername: false,
      isRegistered: false
    };

    this.validateAnswer = this.validateAnswer.bind(this);
    this.setRandomNumber = this.setRandomNumber.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateUsername = this.validateUsername.bind(this);
    this.validatePassword = this.validatePassword.bind(this);

    this.checkUsername = this.checkUsername.bind(this);

    this.goodUsername = this.goodUsername.bind(this);
    this.wrongUsername = this.wrongUsername.bind(this);
    this.resetWarningInput = this.resetWarningInput.bind(this);
    this.registered = this.registered.bind(this);
    this.registerUser = this.registerUser.bind(this);
    this.failedRegistration = this.failedRegistration.bind(this);
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeAnswer = this.handleChangeAnswer.bind(this);
  }

  handleChangeUsername(event) {
    const target = event.target;
    const name = target.name;
    this.setState({ [name]: event.target.value }, this.validateUsername);
  }

  handleChangePassword(event) {
    const target = event.target;
    const name = target.name;
    this.setState({ [name]: event.target.value }, this.validatePassword);
  }

  handleChangeAnswer(event) {
    const target = event.target;
    const name = target.name;
    this.setState({ [name]: event.target.value }, this.validateAnswer);
  }

  validateAnswer() {
    let userAnswer = parseInt(this.state.answer, 10)
    userAnswer === this.state.randomNumber * 2
      ? this.setState({
          answerInvalid: false,
          answerValid: true,
          answerWarning: "",
          registerWarning: ""
        })
      : this.setState({
          answerInvalid: true,
          answerValid: false,
          answerWarning: "Your answer is not correct",
          registerWarning: ""
        });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.resetWarningInput();
    this.validateForm();
  }

  setRandomNumber() {
    let date = Date.now().toString();
    let number = date.slice(date.length - 1, date.length);
    this.setState({ randomNumber: number });
  }

  resetWarningInput() {
    this.setState({
      userClass: "input input-username",
      warning: ""
    });
  }

  wrongUsername() {
    this.setState({
      usernameWarning: "User already registered",
      isLoading: false,
      usernameInvalid: true,
      usernameValid: false,
      isLoadingUsername: false,
      registerWarning: ""
    });
    return;
  }

  goodUsername() {
    this.setState({
      usernameWarning: "",
      isLoading: false,
      usernameInvalid: false,
      usernameValid: true,
      isLoadingUsername: false,
      registerWarning: ""
    });
    return;
  }

  validateUsername() {
    if (this.state.username.match(/^[a-z0-9]+$/i) === null) {
      this.setState({
        usernameWarning: "Use only alphanumeric characters",
        usernameInvalid: true,
        usernameValid: false
      });
    } else if (this.state.username.length < 4) {
      this.setState({
        usernameWarning: "Your username is too short",
        usernameInvalid: true,
        usernameValid: false
      });
    } else if (this.state.username.length > 17) {
      this.setState({
        usernameWarning: "Your username is too long",
        usernameInvalid: true,
        usernameValid: false,
        registerWarning: ""
      });
    } else {
      this.checkUsername();
    }
  }

  checkUsername() {
    this.setState({ isLoadingUsername: true });
    sendPost(
      "https://ventpuff.com/register/check",
      {
        username: this.state.username
      },
      this.goodUsername,
      this.wrongUsername
    );
  }

  validatePassword() {
    if (this.state.password.length < 8) {
      this.setState({
        passwordInvalid: true,
        passwordValid: false,
        passwordWarning: "Your password is too short",
        registerWarning: ""
      });
    } else {
      this.setState({
        passwordWarning: "",
        passwordInvalid: false,
        passwordValid: true
      });
      if (this.state.password === this.state.repeatedPassword) {
        this.setState({
          repeatedPasswordInvalid: false,
          repeatedPasswordValid: true,
          repeatedPasswordWarning: "",
          registerWarning: ""
        });
      } else {
        if (this.state.password.length <= this.state.repeatedPassword.length) {
          this.setState({
            repeatedPasswordInvalid: true,
            repeatedPasswordValid: false,
            repeatedPasswordWarning: "Your passwords didn't match",
            registerWarning: ""
          });
        }
      }
    }
  }

  registerUser() {
    if (
      (this.state.isLoading === false) &
      (this.state.isRegistered === false)
    ) {
      if (
        this.state.usernameValid &
        this.state.passwordValid &
        this.state.answerValid
      ) {
        this.setState({ isLoading: true });
        sendPost(
          "https://ventpuff.com/register",
          {
            username: this.state.username,
            password: this.state.password
          },
          this.registered,
          this.failedRegistration
        );
      } else {
        this.setState({
          registerWarning: "Error, please, check all fields",
          registerClass: "invalid"
        });
      }
    }
  }

  registered() {
    this.setState({
      registerWarning: "Success! You will be redirected",
      registerClass: "valid",
      isLoading: false,
      isRegistered: true
    });
    setTimeout(this.props.loggUser, 2000);
  }

  failedRegistration() {
    this.setState({
      registerWarning: "Error, please try again",
      registerClass: "invalid",
      isLoading: false
    });
  }

  componentDidMount() {
    this.setRandomNumber();
  }

  render() {
    return (
      <div className="register-wrapper">
        <h2 className="top-title">Sign up</h2>
        {this.props.isLogged ? <Redirect to="/posts/new" /> : null}
        <RegisterForm
          username={this.state.username}
          password={this.state.password}
          repeatedPassword={this.state.repeatedPassword}
          randomNumber={this.state.randomNumber}
          answer={this.state.answer}
          handleSubmit={this.handleSubmit}
          validateAnswer={this.validateAnswer}
          isLoading={this.state.isLoading}
          warning={this.state.warning}
          usernameValid={this.state.usernameValid}
          usernameInvalid={this.state.usernameInvalid}
          usernameWarning={this.state.usernameWarning}
          passwordValid={this.state.passwordValid}
          passwordInvalid={this.state.passwordInvalid}
          passwordWarning={this.state.passwordWarning}
          repeatedPasswordValid={this.state.repeatedPasswordValid}
          repeatedPasswordInvalid={this.state.repeatedPasswordInvalid}
          repeatedPasswordWarning={this.state.repeatedPasswordWarning}
          answerValid={this.state.answerValid}
          answerInvalid={this.state.answerInvalid}
          answerWarning={this.state.answerWarning}
          registerWarning={this.state.registerWarning}
          validateUsername={this.validateUsername}
          validatePassword={this.validatePassword}
          handleChangeUsername={this.handleChangeUsername}
          handleChangePassword={this.handleChangePassword}
          handleChangeAnswer={this.handleChangeAnswer}
          isLoadingUsername={this.state.isLoadingUsername}
          registerUser={this.registerUser}
          registerClass={this.state.registerClass}
        />

        <Policy />
      </div>
    );
  }
}

export default Register;
