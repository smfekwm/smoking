import React from "react";
import TextareaAutosize from "react-textarea-autosize";

class Radio extends React.Component {
  render() {
    return (
      <div>
        <label className="report-radio-wrapper" >
          Spam
          <input type="radio" defaultChecked="checked" name="reportType" value="spam"
          onChange={this.props.handleChange}/>
          <span className="check-radio" />
        </label>
        <label className="report-radio-wrapper"  >
          Harrassment
          <input type="radio" name="reportType"  value="assault"
          onChange={this.props.handleChange}/>
          <span className="check-radio" />
        </label>
        <label className="report-radio-wrapper" >
          Other
          <input type="radio" name="reportType" value="other"
          onChange={this.props.handleChange}/>
          <span className="check-radio" />
        </label>
      </div>
    );
  }
}

class Report extends React.Component {
  componentDidMount() {
    this.props.hideContextMenu(this.props.showReport)
  }
  render() {
    return (
        <div className="input-box-expanded">
          {this.props.reportWasSent
          ? <div>
            
          <i className="fas fa-times" onClick={this.props.showReport}
          />
            <h4>Your report has been sent to verify.</h4>
          </div>
          : <div className="textarea-expanded">
          <Radio onChange={this.props.handleChange}/>
          <form
            className="post-input-expanded"
            onSubmit={this.props.handleSubmitReport}
          >
            <TextareaAutosize
              className="textarea-expanded"
              minRows={2}
              maxRows={6}
              resize="none"
              name="textFormValueReport"
              autoComplete="off"
              placeholder="Write your report..."
              value={this.props.value}
              onChange={this.props.handleChange}
            />
          </form>
          <div className="buttons-post-box">
          <a
            className="button-default half grey"
            onClick={this.props.showReport}
          >Cancel</a>
          <a
            className="button-default half"
            onClick={this.props.handleSubmitReport}
          >
            {this.props.isPosting ? (
              <i className="fa fa-spinner fa-spin" />
            ) : (
              "Submit"
            )}
          </a>
          </div>
          </div>

          }

        </div>
    );
  }
}

export default Report;
