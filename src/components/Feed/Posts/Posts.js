import React from "react";
import "./Posts.css";
import Comments from "./Comments/Comments";
import Like from "./Like/Like";
import EditForm from "./Edit/EditForm";
import moment from "moment";
import BasicInput from "./Comments/Input/BasicInput";
import Options from "./Options/Options"
import Report from "./Report/Report"
import Status from "./Status"
import { NavLink } from "react-router-dom";
import {sendPost} from '../../../functions.js';




class NewsText extends React.Component {
  
  render() {
    return (
      <div className="post-text">
            <p><b>{this.props.medium}</b>: <a className="post-link" href={this.props.link} target="blank">{this.props.text}</a></p>
          </div>
    );
  }
}


class PostsText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLong: false,
      isExpanded: false
    };
    this.isExpanded = this.isExpanded.bind(this);
  }

  isLong() {
    this.props.text.length > 300
      ? this.setState({ isLong: true })
      : this.setState({ isLong: false });
  }

  isExpanded() {
    this.state.isExpanded
      ? this.setState({ isExpanded: false, isLong: true })
      : this.setState({ isExpanded: true, isLong: false });
  }

  componentDidMount() {
    this.isLong();
  }
  render() {
    return (
      <div className="post-text">
        {this.state.isLong ? (
          <div>
            {this.props.text.slice(
              0,
              this.props.text.slice(0, 600).lastIndexOf(" ")
            )}
            {"... "}
            <div className="show-more" onClick={this.isExpanded}>
              (more)
            </div>
          </div>
        ) : (
          this.props.text
        )}
      </div>
    );
  }
}


class Image extends React.Component {
  render() {
    return (
      <div className="post-image">
        <img alt="post" src={this.props.image} />
      </div>
    );
  }
}

class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showComments: false,
      showLikes: false,
      optionsAreVisible: false,
      isEdited: false,
      showReport: false,
      deleteDialogIsVisible: false,
      textFormValueReport: "",
      reportType: "",
      reportWasSent: false,
    };

    this.confirmDelete = this.confirmDelete.bind(this);
    this.postDelete = this.postDelete.bind(this);
    this.isEdited = this.isEdited.bind(this);
    this.makeLike = this.makeLike.bind(this);
    this.showReport = this.showReport.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmitReport = this.handleSubmitReport.bind(this);
    this.blockUser = this.blockUser.bind(this);
    this.showComments = this.showComments.bind(this);
    this.showOptions = this.showOptions.bind(this);
    this.hideContextMenu = this.hideContextMenu.bind(this);
    this.reportResponse = this.reportResponse.bind(this);

  }
  showComments() {
    this.state.showComments
      ? this.setState({
          showComments: false
        })
      : this.setState({ showComments: true });
  }

  makeLike() {
    sendPost("https://ventpuff.com/like", { id: this.props.postId }, this.props.updateData);
  }

  handleSubmitReport(event) {
    event.preventDefault();
    let contentType = "post"
    let id = this.props.postId
    let username = this.props.username
    let text = this.props.text
    let report = this.state.textFormValueReport
    let reportType = this.state.reportType
    sendPost("https://ventpuff.com/report", { contentType: contentType, id: id, username: username, text: text, report: report, reportType: reportType }, this.reportResponse);
  }

  blockUser() {
    let username = this.props.username
    sendPost("https://ventpuff.com/block", { username: username}, this.props.loadNewData);

  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    
    this.setState({ [name]: event.target.value });
    
  }

  hideContextMenu (callback) {
    document.querySelector(".post-options-menu-wrapper").addEventListener("click", function(event) {
    if (event.target !==  this) {
      return
    }
    callback.call();
    })
  }

  reportResponse() {
    this.setState({ reportWasSent: true })
  }

  isEdited() {
    this.state.isEdited
      ? this.setState({
          isEdited: false
        })
      : this.setState({ isEdited: true, optionsAreVisible: false  });
  }

  confirmDelete() {
    this.state.deleteDialogIsVisible
      ? this.setState({ deleteDialogIsVisible: false})
      : this.setState({ deleteDialogIsVisible: true});
  }


  postDelete() {
    sendPost("https://ventpuff.com/delete", { postId: this.props.postId }, this.props.updateData);
  }
  showReport() {
    this.state.showReport
      ? this.setState({ showReport: false})
      : this.setState({ showReport: true, optionsAreVisible: false });
  }

  showOptions() {
    this.state.optionsAreVisible 
    ? this.setState({ optionsAreVisible: false, deleteDialogIsVisible: false })
    : this.setState({ optionsAreVisible: true })
  }

  render() {
    let postTag = "";
    {this.props.tag !== "null" 
    ? postTag= `/posts/${this.props.tag}`
    : null
  }
    const comments = this.props.comments.map(comment => (
      <Comments
        key={comment.id}
        commentId={comment.id}
        commentText={comment.text}
        commentUsername={comment.username}
        usernameToProfile={`/profile/${comment.username}`}
        commentDate={moment(parseInt(comment.timestamp, 10))
          .startOf("minutes")
          .fromNow()}
        user={this.props.user}
        isLogged={this.props.isLogged}
        updateData={this.props.updateData}
        likes={comment.commentLikes}
        liked={comment.commentLikes !==  null ?
        ((comment.commentLikes.map(like => like.username)).includes(this.props.user)
        ? true
        : false
        )
        : false   
        }
        subComments={comment.subComments !==  null ? comment.subComments : []}
        loadNewData={this.props.loadNewData}
      />
    ));

    return (
  
      <div className="post-box">
      {this.state.optionsAreVisible
             ? <Options 
             user={this.props.user}
             username={this.props.username}
             isEdited={this.isEdited}
             confirmDelete={this.confirmDelete}

              showReport={this.showReport}
              blockUser={this.blockUser}
              showOptions={this.showOptions}
              hideContextMenu={this.hideContextMenu}
              deleteDialogIsVisible={this.state.deleteDialogIsVisible}
              postDelete={this.postDelete}
             />
             : null
             }

      {this.state.showReport
             ? <div className="post-options-menu-wrapper">
              <Report 
             user={this.props.user}
             handleSubmitReport={this.handleSubmitReport}
             handleChange={this.handleChange}
             textFormValueReport={this.props.textFormValueReport}
             showReport={this.showReport}
             reportWasSent={this.state.reportWasSent}
             hideContextMenu={this.hideContextMenu}
             /> </div>
             : null
             }
             
        <div className="post-header">
          <img alt = "profile avatar" className="post-avatar" src={this.props.avatar} />

          <div className="post-user">
          <NavLink className="post-username link-big" to={this.props.usernameToProfile}>
            {this.props.username}
            </NavLink>
          {this.props.smokerStatus ? (
            <Status
              smokerStatus={this.props.smokerStatus}
              quitDate={this.props.quitDate}
            />
          ) : null}
          </div>
      
          <div className="post-date">
            {this.props.timestamp.slice(0, this.props.timestamp.indexOf("ago"))}
              {this.props.user
          ?  <i className="fas fa-ellipsis-h post-options" onClick={this.showOptions}></i>
             : null}
             
             
          </div>
         
        </div>
        {this.props.title
        ? <p className="post-title">{this.props.title}</p>
        : null
        }

        {this.props.image ? <Image image={this.props.image} /> : null}

        {this.state.isEdited ? (
          <div className="post-text">
         
            <EditForm
              postText={this.props.text}
              postId={this.props.postId}
              updateData={this.props.updateData}
              isEdited={this.isEdited}
            />
          </div>
        ) : (
          <div>
          {this.props.tag === "news"
          ? <NewsText medium={this.props.text.slice(0,  this.props.text.indexOf(":"))}
          link={this.props.text.slice(this.props.text.indexOf("http"))}
          text={this.props.text.slice(
              this.props.text.indexOf(":") + 2,
              this.props.text.indexOf("http")
            )} />
          : <PostsText text={this.props.text} />
          }
          </div>
        )}
        {this.props.tag !==  "null" 
        ?(
          <NavLink className="post-tag" activeClassName="" to={postTag}>#{this.props.tag}</NavLink>
                    ) : null}
        <div className="post-footer">
          <div className="post-likes">
            {this.props.user !==  null ? (
              <div className="post-heart">
                <Like
                makeLike={this.makeLike.bind(this)}
                  liked={this.props.liked}
                />
              </div>
            ) : (
              <div className="post-heart">
                <Like/>
              </div>
            )}
            {this.props.likes ? (
              <div className="post-likes-text">{this.props.likes.length}</div>
            ) : null}
          </div>
          <div className="post-comment-list" onClick={this.showComments}>
            <i className="far fa-comment" />
            {"  "}
              {this.props.comments.length > 0
                ?  <div className="post-likes-text"> {this.props.comments.length}</div>
                : null}
          </div>
 
        </div>


        {this.state.showComments ? (
          this.props.comments === null ? (
            <div className="input-box">
              <BasicInput
                route="https://ventpuff.com/comment"
                isLogged={this.props.isLogged}
                updateData={this.props.updateData}
                postId={this.props.postId}
                user={this.props.user}
                textAreaClass={"textarea-post"}
              />
            </div>
          ) : (
            <div>
              <div className="input-box">
                <BasicInput
                  route="https://ventpuff.com/comment"
                  isLogged={this.props.isLogged}
                  updateData={this.props.updateData}
                  postId={this.props.postId}
                  user={this.props.user}
                  textAreaClass={"textarea-post"}
                />
              </div>
              <div className="dashed-div"></div>
       {comments}
            </div>
          )
        ) : null}
      </div>
    );
  }
}

export default Posts;
