import React, { Component } from 'react';
import {Route} from 'react-router-dom'
import Home from './components/Home/Home'
import Navbar from './components/Navbar/Navbar'
import Feed from './components/Feed/Feed'
import Notifications from './components/Notifications/Notifications'
import Post from './components/Post/Post'
import Profile from './components/Profile/Profile'
import Register from './components/Profile/Register/Register'
import Login from './components/Profile/Login/Login'
import Settings from './components/Settings/Settings'
import Terms from './components/Terms/Terms'

import {sendPost} from './functions.js';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      smokerStatus: "",
      quitDate: "",
      userData: {},
      isLogged: false,
      isLoading: false,
      isMobile: false,
      notificationPost: null,
      selectedNotification: null,
      notificationsCount: null,
      navbarClass: "",

    };
    this.setLoading=this.setLoading.bind(this);
    this.loggUser=this.loggUser.bind(this);
    this.isMobile=this.isMobile.bind(this);
    this.openNotification=this.openNotification.bind(this);
    this.updateData=this.updateData.bind(this);

    this.checkNewNotifications=this.checkNewNotifications.bind(this);
  }
  
  setLoading () {
    this.state.isLoading
    ? this.setState({ isLoading: false })
    : this.setState({ isLoading: true })
  }



  loggUser() {
    const urlUser = "https://ventpuff.com/check/user";
 
    this.setState({ isLoading: true })
    fetch(urlUser, 
      {method: 'GET',
      credentials: 'include',
    })
      .then(response => response.json())
      .then(
        user =>
        user.session 
            ? this.setState({isLoading: false, user: user.session, userData: user.userData, smokerStatus: user.smokerStatus, quitDate: user.quitDate, isLogged: true })
            : this.setState({isLoading: false, user: null, userData: {}, smokerStatus: "", quitDate: "", isLogged: false })
      );
  }
  
  isMobile () {
    let width = window.innerWidth;
    if (width < 600) {
      this.setState({ isMobile: true, navbarClass: "navbar-mobile"
    })
    } else {
      this.setState({ isMobile: false, navbarClass: "navbar-desktop" })
    }
  }

  checkNewNotifications() {
    const url = "https://ventpuff.com/check/notifications";
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ notificationsCount: dataset.count }));

  }

  openNotification(event) {
    let notificationId = event.currentTarget.getAttribute("notificationid")
    let parrentId = event.currentTarget.getAttribute("parrentid")
    let type = event.currentTarget.getAttribute("type")
    let parrentsubcomment = event.currentTarget.getAttribute("parrentsubcomment")
    sendPost("https://ventpuff.com/view/notification", {notificationId: notificationId, parrentId: parrentId, type: type, parrentsubcomment: parrentsubcomment}, this.checkNewNotifications())

  }




  updateData() {
    let postId = this.state.selectedNotification;
    const url = `https://ventpuff.com/update/notification/id/${postId}`;
    fetch(url, {
      method: "GET",
      credentials: "include"
    })
      .then(response => response.json())
      .then(dataset => this.setState({ notificationPost: dataset}));
  }

  componentDidMount() {
    this.loggUser();
    this.isMobile();
    this.checkNewNotifications();
    this.interval = setInterval(() => this.checkNewNotifications(), 50000);
    window.addEventListener("resize", this.isMobile);

  }

  componentWillUnmount() {
    clearInterval(this.interval);
    window.removeEventListener("resize", this.isMobile);

  }

  render() {
   
    return (
      
      <div className="react-container">
      <Navbar navbarClass={this.state.navbarClass} loggUser={this.loggUser} user={this.state.user}
        notificationsCount={this.state.notificationsCount}
        isLogged={this.state.isLogged}
        openNotification={this.openNotification}
        isMobile={this.state.isMobile}
        />
       
 

        <Route activeClassName="active" exact path="/home" component={Home} />
        <Route activeClassName="active" exact path="/terms" component={Terms} />
        <Route exact path="/posts/:type" render={(routerProps) => <Feed isMobile={this.state.isMobile} user={this.state.user} isLogged={this.state.isLogged} 
        setLoading={this.setLoading} 
        key={routerProps.location.key}
        {...routerProps} />} />
        <Route exact path="/" render={(routerProps) => <Feed isMobile={this.state.isMobile} user={this.state.user} isLogged={this.state.isLogged} 
        setLoading={this.setLoading} 
        key={routerProps.location.key}
        {...routerProps} />} />
        <Route exact path="/notifications" render={() => <Notifications isMobile={this.state.isMobile} user={this.state.user} isLogged={this.state.isLogged} openNotification={this.openNotification}
        setLoading={this.setLoading} 
        checkNewNotifications={this.checkNewNotifications}/>} 
         />
        <Route exact path="/profile/:userProfile" render={(routerProps) => <Profile 
        isLogged={this.state.isLogged} 
        loggUser={this.loggUser}
        user={this.state.user} 
        {...routerProps} />} />
        <Route exact path="/settings/:settingsType" render={(routerProps) => <Settings 
        loggUser={this.loggUser}
        isLogged={this.state.isLogged} user={this.state.user} {...routerProps} />} />
        <Route exact path="/register" render={() => <Register 
        loggUser={this.loggUser} isLogged={this.state.isLogged}/>} />
        <Route exact path="/login" render={() => <Login isLogged={this.state.isLogged}isLoading={this.state.isLoading} loggUser={this.loggUser} user={this.state.user}/>} />
        <Route path="/post/:postId" render={(routerProps) => <Post 
        key={routerProps.location.key}
        isLogged={this.state.isLogged} 
        loggUser={this.loggUser}
        user={this.state.user} 
        checkNewNotifications={this.checkNewNotifications}
       
        {...routerProps} />} />
      </div>


    );
  }
}

export default App;
