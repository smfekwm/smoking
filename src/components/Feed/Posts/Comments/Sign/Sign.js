import React from "react";
import "./Sign.css";
import { Link } from 'react-router-dom'

class Sign extends React.Component {
  render() {
    return (
      <div className="signing-box">
        <p className="sign-text">Welcome to Smoker Feed. If you want to just browse, feel free to continue. If you would like to post, you have to <Link className={"sign-link"} to="register"> register </Link> and  
        <Link className={"sign-link"} to="login"> login </Link>
        before</p>
       
      </div>
    )
  }
}



export default Sign;
